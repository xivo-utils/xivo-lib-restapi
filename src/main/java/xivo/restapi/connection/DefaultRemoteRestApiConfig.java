package xivo.restapi.connection;

import xivo.restapi.connection.managers.LineManager;

import java.util.HashMap;
import java.util.Map;

public class DefaultRemoteRestApiConfig implements RestapiConfig {

    private Map<String, String> params;
    private final String Version = "1.1";

    public DefaultRemoteRestApiConfig(String host) {
        params = new HashMap<String, String>();
        params.put(EXTENSIONS_URL_FIELD, "/extensions");
        params.put(USERS_URL_FIELD, "/users");
        params.put(LINES_URL_FIELD, "/lines");
        params.put(SIP_ENDPOINTS_URL_FIELD, "/endpoints/sip");
        params.put(VOICEMAILS_URL_FIELD, "/voicemails");
        params.put(CTI_PROFILES_URL_FIELD, "/cti_profiles");
        params.put(DEVICES_URL_FIELD, "/devices");
        params.put(CONFIGURATION_URL_FIELD, "/configuration");
        params.put(RESTAPI_URL, "https://" + host + ":" + port + "/" + Version);
        params.put(EXTERNAL_CONTEXT_NAME, LineManager.EXTERNAL_CONTEXT);
    }

    public String get(String name) {
        return params.get(name);
    }

}
