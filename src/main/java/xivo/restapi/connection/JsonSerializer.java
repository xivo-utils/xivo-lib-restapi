package xivo.restapi.connection;

import java.io.IOException;
import java.util.List;

import xivo.restapi.model.User;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class JsonSerializer {

    public class ResponseContent<T> {
        private int total;
        private List<T> items;

        public List<T> getItems() {
            return items;
        }

        public int getTotal() {
            return total;
        }
    }

    private class CreationReply {
        private Integer id;
        @SuppressWarnings("unused")
        private transient Object links;

        public Integer getId() {
            return id;
        }
    }

    private Gson genericSerializer;
    private Gson putSerializer;

    public JsonSerializer() {
        genericSerializer = new Gson();
        putSerializer = new GsonBuilder().serializeNulls().create();
    }

    public String serializePost(Object object) {
        return genericSerializer.toJson(object);
    }

    public String serializePut(Object object) {
        return putSerializer.toJson(object);
    }

    public List<User> deserializeUsers(String jsonUsers) throws IOException {
        return deserializeList(jsonUsers, new TypeToken<ResponseContent<User>>() {
        });
    }

    public int extractId(String creationReply) {
        CreationReply result = genericSerializer.fromJson(creationReply, CreationReply.class);
        return result.getId();
    }

    public <T> T deserializeObject(String json, Class<T> clazz) {
        return genericSerializer.fromJson(json, clazz);
    }

    public <T> List<T> deserializeList(String json, TypeToken<ResponseContent<T>> token) {
        ResponseContent<T> result = genericSerializer.fromJson(json, token.getType());
        return result.getItems();
    }

    public int extractTotal(String json) {
        ResponseContent<Object> res = genericSerializer.fromJson(json, new TypeToken<ResponseContent<Object>>() {
        }.getType());
        return res.getTotal();
    }
}
