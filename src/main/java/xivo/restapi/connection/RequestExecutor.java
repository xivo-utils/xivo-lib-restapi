package xivo.restapi.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.inject.Inject;

public class RequestExecutor {

    private DefaultHttpClient client;
    private Logger logger;

    @Inject
    public RequestExecutor(DefaultHttpClient client) {
        this.client = client;
        logger = Logger.getLogger(getClass().getName());
    }

    public String fillAndExecuteRequest(HttpEntityEnclosingRequestBase request, String content)
            throws IOException, RequestException {
        HttpEntity body = new StringEntity(content, org.apache.http.Consts.UTF_8);
        request.setEntity(body);
        return executeRequest(request);
    }

    public String executeRequest(HttpRequestBase request) throws IOException, RequestException {
        setRequestHeaders(request);
        HttpResponse response = client.execute(request);
        int status = response.getStatusLine().getStatusCode();
        if (isStatusKo(status)) {
            String errorMessage = getErrorMessage(response);
            throw new RequestException(errorMessage, status);
        }
        if (status != HttpStatus.SC_NO_CONTENT) {
            HttpEntity entity = response.getEntity();
            String returnedValue = streamToString(entity.getContent());
            EntityUtils.consume(entity);
            return returnedValue;
        }
        return "";
    }

    private String getErrorMessage(HttpResponse response) throws IOException {
        HttpEntity entity = response.getEntity();
        String message = response.getStatusLine() + "\n";
        if (entity == null) {
            logger.severe("Request return nothing");
        } else {
            BufferedReader buff = new BufferedReader(new InputStreamReader(entity.getContent()));
            while (buff.ready()) {
                message += buff.readLine();
            }
            buff.close();
            logger.warning(message);
        }
        return message;
    }

    private String streamToString(InputStream stream) throws IOException {
        StringBuffer content = new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        // TODO find why reader.ready() returns false
        String line = reader.readLine();
        while (line != null) {
            content.append(line);
            line = reader.readLine();
        }
        reader.close();
        return content.toString();
    }

    private void setRequestHeaders(HttpRequest request) {
        request.setHeader("Content-Type", "application/json");
        request.setHeader("Accept", "application/json");
    }

    private boolean isStatusKo(int status) {
        return status < HttpStatus.SC_OK || status >= HttpStatus.SC_MULTIPLE_CHOICES;
    }
}
