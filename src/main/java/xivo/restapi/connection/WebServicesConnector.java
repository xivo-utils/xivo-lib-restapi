package xivo.restapi.connection;

import java.io.IOException;
import java.util.List;

import xivo.restapi.connection.managers.ConfigurationManager;
import xivo.restapi.connection.managers.CtiProfileManager;
import xivo.restapi.connection.managers.DeviceManager;
import xivo.restapi.connection.managers.LineManager;
import xivo.restapi.connection.managers.UserManager;
import xivo.restapi.connection.managers.VoicemailManager;
import xivo.restapi.model.*;
import xivo.restapi.model.Links.UserCti;
import xivo.restapi.model.Links.UserLine;
import xivo.restapi.model.Links.UserVoicemail;

import com.google.inject.Inject;

public class WebServicesConnector {

    private UserManager userManager;
    private LineManager lineManager;
    private VoicemailManager voicemailManager;
    private CtiProfileManager ctiProfileManager;
    private ConfigurationManager configManager;
    private DeviceManager deviceManager;

    @Inject
    public WebServicesConnector(RequestExecutor executor, JsonSerializer serializer, RestapiConfig config) {
        String xivoBaseURL = config.get(RestapiConfig.RESTAPI_URL);
        userManager = new UserManager(executor, serializer, xivoBaseURL + config.get(RestapiConfig.USERS_URL_FIELD));
        lineManager = new LineManager(executor, serializer, xivoBaseURL + config.get(RestapiConfig.LINES_URL_FIELD),
                xivoBaseURL + config.get(RestapiConfig.EXTENSIONS_URL_FIELD),
                xivoBaseURL + config.get(RestapiConfig.SIP_ENDPOINTS_URL_FIELD),
                config.get(RestapiConfig.EXTERNAL_CONTEXT_NAME));
        voicemailManager = new VoicemailManager(executor, serializer, xivoBaseURL
                + config.get(RestapiConfig.VOICEMAILS_URL_FIELD));
        ctiProfileManager = new CtiProfileManager(executor, serializer, xivoBaseURL
                + config.get(RestapiConfig.CTI_PROFILES_URL_FIELD));
        configManager = new ConfigurationManager(executor, serializer, xivoBaseURL,
                config.get(RestapiConfig.CONFIGURATION_URL_FIELD));
        deviceManager = new DeviceManager(executor, serializer, xivoBaseURL
                + config.get(RestapiConfig.DEVICES_URL_FIELD));
    }

    public synchronized User createUser(User user) throws WebServicesException, IOException {
        User createdUser = userManager.create(user);
        userManager.updateCtiConfiguration(createdUser);
        return createdUser;
    }

    public synchronized List<User> getAllUsers() throws IOException, WebServicesException {
        List<User> users = userManager.list();
        for (User user : users) {
            fillUser(user);
        }
        return users;
    }

    private void fillUser(User user) throws IOException, WebServicesException {
        int userid = user.getId();
        user.setVoicemail(getVoicemailForUser(userid));
        List<UserLine> uls = userManager.getUserLines(userid);
        if (uls.size() > 0)
            lineManager.setLineAndIncall(user, uls.get(0).line_id);
        user.setCtiConfiguration(getCtiConfigForUser(userid));
    }

    public synchronized void updateUser(User user) throws IOException, WebServicesException {
        userManager.update(user);
        userManager.updateCtiConfiguration(user);
    }

    public synchronized void updateVoicemail(User user) throws IOException, WebServicesException {
        // for the moment Rest API does not allow to update a voicemail which is
        // associated to a user (should be solved soon)
        userManager.dissociateVoicemail(user);
        voicemailManager.update(user.getVoicemail());
        userManager.associateVoicemail(user);
    }

    public synchronized void createLine(Line line) throws IOException, WebServicesException {
        lineManager.create(line);
    }

    public synchronized void associateLineToUser(User user) throws IOException, WebServicesException {
        userManager.associateLine(user);
    }

    public synchronized void updateLine(Line line) throws IOException, WebServicesException {
        lineManager.update(line);
    }

    public synchronized void deleteLine(Line line) throws IOException, WebServicesException {
        if (line.getDeviceId() != null) {
            deviceManager.resetAutoprov(new Device(line.getDeviceId()));
        }
        lineManager.delete(line);
    }

    public synchronized Line getLineForUser(int userId) throws IOException, WebServicesException {
        List<UserLine> userLines = userManager.getUserLines(userId);
        if (userLines.size() == 0)
            return null;
        return lineManager.get(userLines.get(0).line_id);
    }

    public synchronized void createVoicemail(Voicemail voicemail) throws IOException, WebServicesException {
        voicemailManager.create(voicemail);
    }

    public synchronized void dissociateLineFromUser(User user) throws IOException, WebServicesException {
        try {
            if (user.getLine().getDeviceId() != null) {
                Device device = new Device(user.getLine().getDeviceId());
                deviceManager.resetAutoprov(device);
                deviceManager.synchronize(device);
            }
        } catch (WebServicesException e) {
            if (e.getStatus() != 404)
                throw e;
        }
        try {
            userManager.dissociateLine(user);
        } catch (WebServicesException e) {
            if (e.getStatus() != 404)
                throw e;
        }
    }

    public synchronized void dissociateVoicemailFromUser(User user) throws IOException, WebServicesException {
        try {
            userManager.dissociateVoicemail(user);
        } catch (WebServicesException e) {
            if (e.getStatus() != 404)
                throw e;
        }
    }

    public synchronized void deleteUser(User user) throws WebServicesException, IOException {
        userManager.delete(user);
    }

    public synchronized void deleteVoicemail(Voicemail voicemail) throws WebServicesException, IOException {
        voicemailManager.delete(voicemail);
    }

    public synchronized List<UserVoicemail> getUsersIdsAssociatedToVoicemail(int voicemailId) throws WebServicesException, IOException {
        return voicemailManager.getAssociatedUsersIds(voicemailId);
    }

    public synchronized void associateVoicemailToUser(User user) throws IOException, WebServicesException {
        userManager.associateVoicemail(user);
    }

    public synchronized Voicemail getVoicemailForUser(int userId) throws IOException, WebServicesException {
        UserVoicemail uv = userManager.getUserVoicemail(userId);
        if (uv == null)
            return null;
        return voicemailManager.get(uv.voicemail_id);
    }

    public synchronized List<Voicemail> listVoicemails() throws IOException, WebServicesException {
        return voicemailManager.list();
    }

    public synchronized CtiConfiguration getCtiConfigForUser(int userId) throws IOException, WebServicesException {
        UserCti ucp = userManager.getUserCti(userId);
        CtiProfile ctiProfile = null;
        if (ucp.cti_profile_id != null)
            ctiProfile = ctiProfileManager.getCtiProfileById(ucp.cti_profile_id);
        return new CtiConfiguration(ucp.enabled, ctiProfile);
    }

    public synchronized CtiProfile getCtiProfileByName(String profileName) throws IOException, WebServicesException {
        return ctiProfileManager.getCtiProfileByName(profileName);
    }

    public synchronized void updateCtiConfiguration(User user) throws WebServicesException, IOException {
        userManager.updateCtiConfiguration(user);
    }

    public synchronized void disableLiveReload() throws IOException, WebServicesException {
        configManager.disableLiveReload();
    }

    public synchronized void enableLiveReload() throws IOException, WebServicesException {
        configManager.enableLiveReload();
    }

    public synchronized void resetAutoprov(Device device) throws IOException, WebServicesException {
        deviceManager.resetAutoprov(device);
    }

    public synchronized User getUser(int userId) throws IOException, WebServicesException {
        User user = userManager.get(userId);
        fillUser(user);
        return user;
    }

    public synchronized int getUsersNumber() throws IOException, WebServicesException {
        return userManager.countUsers();
    }

    public synchronized Info getInfo() throws IOException, WebServicesException {
        return configManager.getInfo();
    }

    public synchronized void synchronizeDevice(Device device) throws IOException, WebServicesException {
        deviceManager.synchronize(device);
    }
}
