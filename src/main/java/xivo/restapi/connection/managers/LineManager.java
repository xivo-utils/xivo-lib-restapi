package xivo.restapi.connection.managers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.JsonSerializer.ResponseContent;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.IncomingCall;
import xivo.restapi.model.Line;
import xivo.restapi.model.Line.RestExtension;
import xivo.restapi.model.Line.RestLine;
import xivo.restapi.model.Links;
import xivo.restapi.model.Links.LineExtension;
import xivo.restapi.model.Links.LineEndpoint;
import xivo.restapi.model.User;

import com.google.gson.reflect.TypeToken;

public class LineManager extends Manager {

    public static final String EXTERNAL_CONTEXT = "from-extern";

    private String linesUrl;
    private String extensionsUrl;
    private String sipEndpointUrl;
    private String externalContext;

    public LineManager(RequestExecutor executor, JsonSerializer serializer, String linesUrl, String extensionsUrl,
                       String sipEndpointUrl, String externalContext) {
        super(executor, serializer);
        this.linesUrl = linesUrl;
        this.extensionsUrl = extensionsUrl;
        this.sipEndpointUrl = sipEndpointUrl;
        this.externalContext = externalContext;
    }

    public LineManager(RequestExecutor executor, JsonSerializer serializer, String linesUrl, String extensionsUrl,
                       String sipEndpointUrl) {
        this(executor, serializer, linesUrl, extensionsUrl, sipEndpointUrl, EXTERNAL_CONTEXT);
    }

    public void create(Line line) throws IOException, WebServicesException {
        createLine(line);
        try {
            createExtension(line);
        } catch (WebServicesException e) {
            deleteLine(line);
            throw e;
        }
        try {
            createEndpoint(line);
        } catch (WebServicesException e) {
            deleteExtension(line);
            deleteLine(line);
            throw e;
        }
        try {
            associateLineToEndpoint(line);
        } catch (WebServicesException e) {
            deleteEndpoint(line);
            deleteExtension(line);
            deleteLine(line);
            throw e;
        }
        try {
            associateLineToExtension(line);
        } catch (WebServicesException e) {
            dissociateLineEndpoint(line);
            deleteEndpoint(line);
            deleteExtension(line);
            deleteLine(line);
            throw e;
        }
    }

    private void createExtension(Line line) throws IOException, WebServicesException {
        HttpPost post = new HttpPost(extensionsUrl);
        HashMap<String, Object> body = new HashMap<String, Object>();
        body.put("exten", line.getNumber());
        body.put("context", line.getContext());
        String returnedValue = null;
        try {
            returnedValue = executor.fillAndExecuteRequest(post, serializer.serializePost(body));
        } catch (RequestException e) {
            throw new WebServicesException("POST - create extension : " + e.getMessage(), e.getStatus());
        }
        if (!returnedValue.isEmpty()) {
            int id = serializer.extractId(returnedValue);
            line.setExtensionId(id);
        }
    }

    private void createEndpoint(Line line) throws IOException, WebServicesException {
        HttpPost post = new HttpPost(sipEndpointUrl);
        HashMap<String, Object> body = new HashMap<String, Object>();
        if (line.getName() != null && !line.getName().isEmpty()) {
            body.put("name", line.getName());
        }
        String returnedValue = null;
        try {
            returnedValue = executor.fillAndExecuteRequest(post, serializer.serializePost(body));
        } catch (RequestException e) {
            throw new WebServicesException("POST - create SIP endpoint : " + e.getMessage(), e.getStatus());
        }
        if (!returnedValue.isEmpty()) {
            Line.RestSIPEndpoint rse = serializer.deserializeObject(returnedValue, Line.RestSIPEndpoint.class);
            line.setEndpointId(rse.id);
            line.setName(rse.name);
        }
    }

    private void associateLineToEndpoint(Line line) throws IOException, WebServicesException {
        HttpPut put = new HttpPut(linesUrl + "/" + line.getLineId() + "/endpoints/sip/" + line.getEndpointId());
        Map<String, Object> body = new HashMap<String, Object>();
        body.put("line_id", line.getLineId());
        body.put("endpoint_id", line.getEndpointId());
        try {
            executor.fillAndExecuteRequest(put, serializer.serializePost(body));
        } catch (RequestException e) {
            throw new WebServicesException("POST - associate line to endpoint: " + e.getMessage(), e.getStatus());
        }
    }

    private void createLine(Line line) throws IOException, WebServicesException {
        HttpPost post = new HttpPost(linesUrl);
        String returnedValue = null;
        try {
            returnedValue = executor.fillAndExecuteRequest(post, serializer.serializePost(line.getRestLine()));
        } catch (RequestException e) {
            throw new WebServicesException("POST - create line : " + e.getMessage(), e.getStatus());
        }
        RestLine newRestLine = serializer.deserializeObject(returnedValue, RestLine.class);
        line.update(newRestLine);
    }

    private void associateLineToExtension(Line line) throws IOException, WebServicesException {
        HttpPost post = new HttpPost(linesUrl + "/" + line.getLineId() + "/extension");
        Map<String, Object> body = new HashMap<String, Object>();
        body.put("extension_id", line.getExtensionId());
        try {
            executor.fillAndExecuteRequest(post, serializer.serializePost(body));
        } catch (RequestException e) {
            throw new WebServicesException("POST - associate line to extension : " + e.getMessage(), e.getStatus());
        }
    }

    public void update(Line line) throws IOException, WebServicesException {
        updateLine(line);
        updateExtension(line);
    }

    private void updateExtension(Line line) throws IOException, WebServicesException {
        HttpPut put = new HttpPut(extensionsUrl + "/" + line.getExtensionId());
        Map<String, Object> body = new HashMap<String, Object>();
        body.put("exten", line.getNumber());
        body.put("context", line.getContext());
        try {
            executor.fillAndExecuteRequest(put, serializer.serializePut(body));
        } catch (RequestException e) {
            throw new WebServicesException("PUT - update line : " + e.getMessage(), e.getStatus());
        }
    }

    private void updateLine(Line line) throws IOException, WebServicesException {
        HttpPut put = new HttpPut(linesUrl + "/" + line.getLineId());
        Map<String, Object> body = new HashMap<String, Object>();
        body.put("context", line.getContext());
        body.put("position", line.getposition());
        try {
            executor.fillAndExecuteRequest(put, serializer.serializePut(body));
        } catch (RequestException e) {
            throw new WebServicesException("PUT - update line : " + e.getMessage(), e.getStatus());
        }
    }

    public void delete(Line line) throws IOException, WebServicesException {

        dissociateLineExtension(line);
        dissociateLineEndpoint(line);
        deleteEndpoint(line);
        deleteLine(line);
        deleteExtension(line);
    }

    private void dissociateLineExtension(Line line) throws IOException, WebServicesException {
        try {
            HttpDelete delete = new HttpDelete(linesUrl + "/" + line.getLineId() + "/extension");
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - line extension dissociation : " + e.getMessage(), e.getStatus());
        }
    }

    private void dissociateLineEndpoint(Line line) throws IOException, WebServicesException {
        try {
            HttpDelete delete = new HttpDelete(linesUrl + "/" + line.getLineId() + "/endpoints/sip/" + line.getEndpointId());
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - line endpoint dissociation : " + e.getMessage(), e.getStatus());
        }
    }


    private void deleteExtension(Line line) throws IOException, WebServicesException {
        try {
            HttpDelete delete = new HttpDelete(extensionsUrl + "/" + line.getExtensionId());
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - extension deletion : " + e.getMessage(), e.getStatus());
        }
    }

    private void deleteEndpoint(Line line) throws IOException, WebServicesException {
        try {
            HttpDelete delete = new HttpDelete(sipEndpointUrl + "/" + line.getEndpointId());
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - SIP endpoint deletion : " + e.getMessage(), e.getStatus());
        }
    }

    private void deleteLine(Line line) throws IOException, WebServicesException {
        try {
            HttpDelete delete = new HttpDelete(linesUrl + "/" + line.getLineId());
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - line deletion : " + e.getMessage(), e.getStatus());
        }
    }

    public Line get(int lineId) throws IOException, WebServicesException {
        try {
            RestLine restLine = getRestLine(lineId);
            LineExtension lineExten = getLineExtension(lineId);
            RestExtension restExten = getRestExtension(lineExten.extension_id);
            LineEndpoint lineEndpoint = getLineEndpoint(lineId);
            return new Line(restLine, restExten, lineEndpoint);
        } catch (RequestException e) {
            throw new WebServicesException("GET - get line : " + e.getMessage(), e.getStatus());
        }
    }

    private RestExtension getRestExtension(int extensionId) throws IOException, RequestException {
        HttpGet get = new HttpGet(extensionsUrl + "/" + extensionId);
        String result = executor.executeRequest(get);
        RestExtension restExten = serializer.deserializeObject(result, RestExtension.class);
        return restExten;
    }

    private LineExtension getLineExtension(int lineId) throws IOException, RequestException {
        HttpGet get = new HttpGet(linesUrl + "/" + lineId + "/extension");
        String result = executor.executeRequest(get);
        LineExtension lineExten = serializer.deserializeObject(result, LineExtension.class);
        return lineExten;
    }

    private Links.LineEndpoint getLineEndpoint(int lineId) throws IOException, RequestException {
        HttpGet get = new HttpGet(linesUrl + "/" + lineId + "/endpoints/sip");
        String result = executor.executeRequest(get);
        LineEndpoint lineEndpoint = serializer.deserializeObject(result, LineEndpoint.class);
        return lineEndpoint;
    }


    private RestLine getRestLine(int lineId) throws IOException, RequestException {
        HttpGet get = new HttpGet(linesUrl + "/" + lineId);
        String result = executor.executeRequest(get);
        RestLine restLine = serializer.deserializeObject(result, RestLine.class);
        return restLine;
    }

    public void setLineAndIncall(User user, int lineId) throws IOException, WebServicesException {
        try {
            List<LineExtension> lineExtens = getLineExtensions(lineId);
            LineEndpoint lineEndpoint = getLineEndpoint(lineId);
            for (LineExtension le : lineExtens) {
                RestExtension e = getRestExtension(le.extension_id);
                if (e.context.startsWith(externalContext)) {
                    user.setIncomingCall(new IncomingCall(e));
                } else {
                    RestLine rl = getRestLine(lineId);
                    user.setLine(new Line(rl, e, lineEndpoint));
                }
            }
        } catch (RequestException e) {
            throw new WebServicesException("setLineAndIncall : " + e.getMessage(), e.getStatus());
        }
    }

    private List<LineExtension> getLineExtensions(int lineId) throws IOException, RequestException {
        HttpGet get = new HttpGet(linesUrl + "/" + lineId + "/extensions");
        String result = executor.executeRequest(get);
        List<LineExtension> lineExtens = serializer.deserializeList(result,
                new TypeToken<ResponseContent<LineExtension>>() {
                });
        return lineExtens;
    }
}
