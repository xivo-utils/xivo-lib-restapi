package xivo.restapi.connection.managers;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.JsonSerializer.ResponseContent;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.Links.UserCti;
import xivo.restapi.model.Links.UserLine;
import xivo.restapi.model.Links.UserVoicemail;
import xivo.restapi.model.User;

import com.google.gson.reflect.TypeToken;

public class UserManager extends Manager {

    private String usersUrl;

    public UserManager(RequestExecutor executor, JsonSerializer serializer, String usersUrl) {
        super(executor, serializer);
        this.usersUrl = usersUrl;
    }

    public User get(int id) throws WebServicesException, IOException {
        HttpGet get = new HttpGet(usersUrl + "/" + id);
        try {
            String result = executor.executeRequest(get);
            return serializer.deserializeObject(result, User.class);
        } catch (RequestException e) {
            throw new WebServicesException("GET - get user : " + e.getMessage(), e.getStatus());
        }
    }

    public void update(User user) throws IOException, WebServicesException {
        HttpPut put = new HttpPut(usersUrl + "/" + user.getId());
        try {
            executor.fillAndExecuteRequest(put, serializer.serializePut(user));
        } catch (RequestException e) {
            throw new WebServicesException("PUT - update user : " + e.getMessage(), e.getStatus());
        }
        if (user.entityId != null) {
            setUserEntity(user);
        }
    }

    public User create(User user) throws IOException, WebServicesException {
        HttpPost post = new HttpPost(usersUrl);
        String returnedValue = null;
        try {
            returnedValue = executor.fillAndExecuteRequest(post, serializer.serializePost(user));
        } catch (RequestException e) {
            throw new WebServicesException("POST - create user : " + e.getMessage(), e.getStatus());
        }
        if (!returnedValue.isEmpty()) {
            int id = serializer.extractId(returnedValue);
            user.setId(id);
        }
        if (user.entityId != null) {
            setUserEntity(user);
        }
        return user;
    }

    public List<User> list() throws IOException, WebServicesException {
        HttpGet get = new HttpGet(usersUrl);
        try {
            String result = executor.executeRequest(get);
            return serializer.deserializeUsers(result);
        } catch (RequestException e) {
            throw new WebServicesException("GET - list users : " + e.getMessage(), e.getStatus());
        }
    }

    public void associateVoicemail(User user) throws IOException, WebServicesException {
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("voicemail_id", user.getVoicemail().getId());
        HttpPost post = new HttpPost(usersUrl + "/" + user.getId() + "/voicemail");
        try {
            executor.fillAndExecuteRequest(post, serializer.serializePost(map));
        } catch (RequestException e) {
            throw new WebServicesException("POST - associate voicemail to user : " + e.getMessage(), e.getStatus());
        }
    }

    public void associateLine(User user) throws IOException, WebServicesException {
        HttpPost post = new HttpPost(usersUrl + "/" + user.getId() + "/lines");
        Map<String, Integer> body = new HashMap<String, Integer>();
        body.put("line_id", user.getLine().getLineId());
        try {
            executor.fillAndExecuteRequest(post, serializer.serializePost(body));
        } catch (RequestException e) {
            throw new WebServicesException("POST - associate line to user", e.getStatus());
        }
    }

    public void dissociateLine(User user) throws IOException, WebServicesException {
        HttpDelete delete = new HttpDelete(usersUrl + "/" + user.getId() + "/lines/" + user.getLine().getLineId());
        try {
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - user - line dissociation : " + e.getMessage(), e.getStatus());
        }
    }

    public void delete(User user) throws IOException, WebServicesException {
        HttpDelete delete = new HttpDelete(usersUrl + "/" + user.getId());
        try {
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - user deletion : " + e.getMessage(), e.getStatus());
        }
    }

    public void dissociateVoicemail(User user) throws IOException, WebServicesException {
        HttpDelete delete = new HttpDelete(usersUrl + "/" + user.getId() + "/voicemail");
        try {
            executor.executeRequest(delete);
        } catch (RequestException e) {
            throw new WebServicesException("DELETE - user - voicemail dissociation : " + e.getMessage(), e.getStatus());
        }
    }

    public List<UserLine> getUserLines(int userId) throws IOException, WebServicesException {
        HttpGet get = new HttpGet(usersUrl + "/" + userId + "/lines");
        String result = null;
        try {
            result = executor.executeRequest(get);
        } catch (RequestException e) {
            if (e.getStatus() == HttpStatus.SC_NOT_FOUND)
                return new LinkedList<UserLine>();
            else
                throw new WebServicesException("GET - get user lines : " + e.getMessage(), e.getStatus());
        }
        List<UserLine> userLines = serializer.deserializeList(result, new TypeToken<ResponseContent<UserLine>>() {
        });
        return userLines;
    }

    public UserVoicemail getUserVoicemail(int userId) throws IOException, WebServicesException {
        HttpGet get = new HttpGet(usersUrl + "/" + userId + "/voicemail");
        String result = null;
        try {
            result = executor.executeRequest(get);
        } catch (RequestException e) {
            if (e.getStatus() == HttpStatus.SC_NOT_FOUND)
                return null;
            else
                throw new WebServicesException("GET - get user voicemail : " + e.getMessage(), e.getStatus());
        }
        return serializer.deserializeObject(result, UserVoicemail.class);
    }

    public UserCti getUserCti(int userId) throws WebServicesException, IOException {
        HttpGet get = new HttpGet(usersUrl + "/" + userId + "/cti");
        String result = null;
        try {
            result = executor.executeRequest(get);
        } catch (RequestException e) {
            throw new WebServicesException("GET - get user CTI configuration : " + e.getMessage(), e.getStatus());
        }
        return serializer.deserializeObject(result, UserCti.class);
    }

    public void updateCtiConfiguration(User user) throws WebServicesException, IOException {
        if (user.getCtiConfiguration() != null) {
            HttpPut put = new HttpPut(usersUrl + "/" + user.getId() + "/cti");
            UserCti userCti = new UserCti(user.getCtiConfiguration());
            try {
                executor.fillAndExecuteRequest(put, serializer.serializePut(userCti));
            } catch (RequestException e) {
                throw new WebServicesException("UPDATE - user CTI configuration : " + e.getMessage(), e.getStatus());
            }
        }
    }

    public int countUsers() throws IOException, WebServicesException {
        HttpGet get = new HttpGet(usersUrl + "?limit=1");
        String result = null;
        try {
            result = executor.executeRequest(get);
        } catch (RequestException e) {
            throw new WebServicesException("GET - count users : " + e.getMessage(), e.getStatus());
        }
        return serializer.extractTotal(result);
    }

    public void setUserEntity(User user) throws WebServicesException, IOException {
        if (user.entityId != null) {
            HttpPut put = new HttpPut(usersUrl + "/" + user.getId() + "/entities/" + user.entityId);
            try {
                executor.executeRequest(put);
            } catch (RequestException e) {
                throw new WebServicesException("UPDATE - user entityId: " + e.getMessage(), e.getStatus());
            }
        }
    }
}
