package xivo.restapi.connection.managers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.Info;

public class ConfigurationManager extends Manager {

    private String configurationUrl;
    private String baseUrl;
    private static String infoPath = "/infos";

    public ConfigurationManager(RequestExecutor executor, JsonSerializer jsonSerializer, String baseUrl, String configurationUrl) {
        super(executor, jsonSerializer);
        this.configurationUrl = baseUrl + configurationUrl;
        this.baseUrl = baseUrl;
    }

    public void disableLiveReload() throws IOException, WebServicesException {
        Map<String, Boolean> params = new HashMap<String, Boolean>();
        params.put("enabled", false);
        String json = serializer.serializePut(params);
        HttpPut put = new HttpPut(configurationUrl + "/live_reload");
        try {
            executor.fillAndExecuteRequest(put, json);
        } catch (RequestException e) {
            throw new WebServicesException("PUT - disable live reload : " + e.getMessage(), e.getStatus());
        }
    }

    public void enableLiveReload() throws IOException, WebServicesException {
        Map<String, Boolean> params = new HashMap<String, Boolean>();
        params.put("enabled", true);
        String json = serializer.serializePut(params);
        HttpPut put = new HttpPut(configurationUrl + "/live_reload");
        try {
            executor.fillAndExecuteRequest(put, json);
        } catch (RequestException e) {
            throw new WebServicesException("PUT - enable live reload : " + e.getMessage(), e.getStatus());
        }
    }

    public Info getInfo() throws IOException, WebServicesException {
        HttpGet get = new HttpGet(baseUrl + infoPath);
        try {
            String re = executor.executeRequest(get);
            Info info = serializer.deserializeObject(re, Info.class);
            return info;
        } catch (RequestException e) {
            throw new WebServicesException("GET - requesting infos: " + e.getMessage(), e.getStatus());
        } catch (Exception e) {
            throw new WebServicesException("GET - requesting infos: " + e.getMessage(), 0);
        }
        }

}
