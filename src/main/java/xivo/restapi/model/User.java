package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

public class User {

    private Integer id;
    private String firstname;
    private String lastname;
    private String caller_id;
    private String username;
    private String password;
    private String music_on_hold;
    private String outgoing_caller_id;
    private String mobile_phone_number;
    private String email;
    private String userfield;
    private String timezone;
    private String language;
    private String description;
    private String preprocess_subroutine;
    private Integer ring_seconds;
    private Integer simult_calls;
    private transient Line line;
    private transient Voicemail voicemail;
    private transient IncomingCall incomingCall;
    private transient CtiConfiguration ctiConfiguration;
    public transient Integer entityId = null;

    private void formatCallerid() {
        caller_id = String.format("\"%s %s\"", (firstname == null ? "" : firstname),
                (lastname == null ? "" : lastname));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
        formatCallerid();
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
        formatCallerid();
    }

    public void setNameWithoutCalledIdUpdate(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getCallerid() {
        return caller_id;
    }

    public void setCallerid(String callerid) {
        this.caller_id = callerid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMusiconhold() {
        return music_on_hold;
    }

    public void setMusiconhold(String musiconhold) {
        this.music_on_hold = musiconhold;
    }

    public String getOutcallerid() {
        return outgoing_caller_id;
    }

    public void setOutcallerid(String outcallerid) {
        this.outgoing_caller_id = outcallerid;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMobilephonenumber() {
        return mobile_phone_number;
    }

    public void setMobilephonenumber(String mobilephonenumber) {
        this.mobile_phone_number = mobilephonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserfield() {
        return userfield;
    }

    public void setUserfield(String userfield) {
        this.userfield = userfield;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public Voicemail getVoicemail() {
        return voicemail;
    }

    public void setVoicemail(Voicemail voicemail) {
        this.voicemail = voicemail;
    }

    public IncomingCall getIncomingCall() {
        return incomingCall;
    }

    public void setIncomingCall(IncomingCall incomingCall) {
        this.incomingCall = incomingCall;
    }

    public String getFullname() {
        return firstname + " " + lastname;
    }

    public String getPreprocessSubroutine() {
        return preprocess_subroutine;
    }

    public void setPreprocessSubroutine(String preprocess_subroutine) {
        this.preprocess_subroutine = preprocess_subroutine;
    }

    public CtiConfiguration getCtiConfiguration() {
        return ctiConfiguration;
    }

    public void setCtiConfiguration(CtiConfiguration ctiConfig) {
        this.ctiConfiguration = ctiConfig;
    }

    public Integer getRingSeconds() {
        return ring_seconds;
    }

    public void setRingSeconds(Integer ring_seconds) {
        this.ring_seconds = ring_seconds;
    }

    public Integer getSimultCalls() {
        return simult_calls;
    }

    public void setSimultCalls(Integer simult_calls) {
        this.simult_calls = simult_calls;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (id != null) {
            builder.append("id=");
            builder.append(id);
            builder.append(", ");
        }
        if (firstname != null) {
            builder.append("firstname=");
            builder.append(firstname);
            builder.append(", ");
        }
        if (lastname != null) {
            builder.append("lastname=");
            builder.append(lastname);
            builder.append(", ");
        }
        if (caller_id != null) {
            builder.append("caller_id=");
            builder.append(caller_id);
            builder.append(", ");
        }
        if (username != null) {
            builder.append("username=");
            builder.append(username);
            builder.append(", ");
        }
        if (password != null) {
            builder.append("password=");
            builder.append(password);
            builder.append(", ");
        }
        if (music_on_hold != null) {
            builder.append("music_on_hold=");
            builder.append(music_on_hold);
            builder.append(", ");
        }
        if (outgoing_caller_id != null) {
            builder.append("outgoing_caller_id=");
            builder.append(outgoing_caller_id);
            builder.append(", ");
        }
        if (mobile_phone_number != null) {
            builder.append("mobile_phone_number=");
            builder.append(mobile_phone_number);
            builder.append(", ");
        }
        if (email != null) {
            builder.append("email=");
            builder.append(email);
            builder.append(", ");
        }
        if (userfield != null) {
            builder.append("userfield=");
            builder.append(userfield);
            builder.append(", ");
        }
        if (timezone != null) {
            builder.append("timezone=");
            builder.append(timezone);
            builder.append(", ");
        }
        if (language != null) {
            builder.append("language=");
            builder.append(language);
            builder.append(", ");
        }
        if (description != null) {
            builder.append("description=");
            builder.append(description);
            builder.append(", ");
        }
        if (preprocess_subroutine != null) {
            builder.append("preprocess_subroutine=");
            builder.append(preprocess_subroutine);
            builder.append(", ");
        }
        if (ring_seconds != null) {
            builder.append("ring_seconds=");
            builder.append(ring_seconds);
            builder.append(", ");
        }
        if (simult_calls != null) {
            builder.append("simult_calls=");
            builder.append(simult_calls);
            builder.append(", ");
        }
        if (line != null) {
            builder.append("line= { ");
            builder.append(line);
            builder.append("}");
            builder.append(", ");
        }
        if (voicemail != null) {
            builder.append("voicemail= { ");
            builder.append(voicemail);
            builder.append("}");
            builder.append(", ");
        }
        if (incomingCall != null) {
            builder.append("incomingCall= { ");
            builder.append(incomingCall);
            builder.append("}");
            builder.append(", ");
        }
        if (ctiConfiguration != null) {
            builder.append("ctiConfiguration = { ");
            builder.append(ctiConfiguration);
            builder.append("}");
        }
        if (entityId != null) {
            builder.append(", ");
            builder.append("entityId=");
            builder.append(entityId);
        }
        return builder.toString();
    }

    public void standardizeCallerid() {
        if (caller_id != null) {
            String calleridRegex = "\"([^\"])*\"\\s*(<\\d+>)?";
            if (!caller_id.matches(calleridRegex))
                caller_id = "\"" + caller_id + "\"";
        }
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this, true);
    }
}
