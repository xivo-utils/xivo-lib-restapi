package xivo.restapi.model;

import java.util.UUID;

public class Info {

    private UUID uuid;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Info(String suuid) {
        this.uuid = UUID.fromString(suuid);
    }

}
