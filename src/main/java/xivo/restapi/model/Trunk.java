package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

public class Trunk {

    private String name;
    private String ip;
    private String context;

    public Trunk(String name, String ip, String context) {
        this.name = name;
        this.ip = ip;
        this.context = context;
    }

    public String getContext() {
        return context;
    }

    public String getName() {
        return name;
    }

    public String getDestinationIp() {
        return ip;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this, true);
    }

}
