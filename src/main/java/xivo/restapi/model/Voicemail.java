package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

public class Voicemail {
    private Integer id;
    private String email;
    private String language;
    private String name;
    private String number;
    private String password;
    private Boolean attach_audio;
    private Boolean ask_password;
    private Boolean delete_messages = false;
    private String context = "default";

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (id != null) {
            builder.append("id=");
            builder.append(id);
            builder.append(", ");
        }
        if (email != null) {
            builder.append("email=");
            builder.append(email);
            builder.append(", ");
        }
        if (name != null) {
            builder.append("name=");
            builder.append(name);
            builder.append(", ");
        }
        if (number != null) {
            builder.append("number=");
            builder.append(number);
            builder.append(", ");
        }
        if (password != null) {
            builder.append("password=");
            builder.append(password);
            builder.append(", ");
        }
        if (attach_audio != null) {
            builder.append("attach_audio=");
            builder.append(attach_audio);
            builder.append(", ");
        }
        if (ask_password != null) {
            builder.append("ask_password=");
            builder.append(ask_password);
            builder.append(", ");
        }
        if (delete_messages != null) {
            builder.append("delete_messages=");
            builder.append(delete_messages);
            builder.append(", ");
        }
        if (context != null) {
            builder.append("context=");
            builder.append(context);
            builder.append(", ");
        }
        // if (language != null) {
        builder.append("language=");
        builder.append(language);
        // }
        return builder.toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAttach() {
        return attach_audio;
    }

    public void setAttach(Boolean attach) {
        this.attach_audio = attach;
    }

    public Boolean getAskPassword() {
        return ask_password;
    }

    public void setAskPassword(Boolean ask_password) {
        this.ask_password = ask_password;
    }

    public Boolean getDeleteMessages() {
        return delete_messages;
    }

    public void setDeleteMessages(Boolean delete_messages) {
        this.delete_messages = delete_messages;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }

}
