package xivo.restapi.model;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;

public class Context {

    private String name;
    private List<ContextInterval> intervals = new LinkedList<ContextInterval>();
    private ContextType type;

    public Context(String name, List<ContextInterval> intervals) {
        this.name = name;
        this.intervals = intervals;
        this.type = ContextType.INTERNAL;
    }

    public Context(String name, List<ContextInterval> intervals, ContextType type) {
        this.name = name;
        this.intervals = intervals;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public List<ContextInterval> getIntervals() {
        return intervals;
    }

    public void addInterval(ContextInterval contextInterval) {
        intervals.add(contextInterval);

    }

    public boolean isNumberInbound(String number) {
        for (ContextInterval it : intervals)
            if (it.isNumberInbound(number))
                return true;
        return false;
    }

    public ContextType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }

    public ContextInterval getIntervalForNumber(String number) {
        for (ContextInterval it : intervals)
            if (it.isNumberInbound(number))
                return it;
        return null;
    }
}
