package xivo.restapi.model;

public class Links {
    public static class UserLine {
        public int line_id;
        public int user_id;
    }

    public static class LineExtension {
        public int line_id;
        public int extension_id;
    }

    public static class LineEndpoint {
        public int line_id;
        public int endpoint_id;
    }

    public static class UserVoicemail {
        public int user_id;
        public int voicemail_id;
    }

    public static class UserCti {
        public UserCti() {
        }

        public UserCti(CtiConfiguration ctiConfiguration) {
            enabled = ctiConfiguration.isEnabled();
            cti_profile_id = ctiConfiguration.getCtiProfile() == null ? null : ctiConfiguration.getCtiProfile().getId();
        }

        public Integer cti_profile_id;
        public Boolean enabled;
    }
}
