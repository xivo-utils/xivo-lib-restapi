package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

public class CtiConfiguration {

    public CtiConfiguration(Boolean enabled, CtiProfile ctiProfile) {
        this.enabled = enabled;
        this.ctiProfile = ctiProfile;
    }

    private CtiProfile ctiProfile;
    private Boolean enabled;

    public CtiProfile getCtiProfile() {
        return ctiProfile;
    }

    public void setCtiProfile(CtiProfile ctiProfile) {
        this.ctiProfile = ctiProfile;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        String res = "";
        if (ctiProfile != null)
            res += "cti_profile=" + ctiProfile.getName() + ",";
        res += "enabled=" + enabled;
        return res;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }
}
