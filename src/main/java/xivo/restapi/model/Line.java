package xivo.restapi.model;

import org.apache.commons.lang.builder.EqualsBuilder;

public class Line {
    private String number;
    private String context = "default";
    private Integer lineId;
    private Integer extensionId;
    private Integer endpointId;
    private Integer position = 1;
    private String provisioningCode;
    private String deviceId;
    private String name;

    public Line() {
    }

    public Line(String number) {
        this.number = number;
    }

    public Line(RestLine restLine, RestExtension restExten, Links.LineEndpoint lineEndpoint) {
        lineId = restLine.id;
        context = restLine.context;
        provisioningCode = restLine.provisioning_code;
        position = restLine.position;
        deviceId = restLine.device_id;
        extensionId = restExten.id;
        number = restExten.exten;
        endpointId = lineEndpoint.endpoint_id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (number != null) {
            builder.append("number=");
            builder.append(number);
            builder.append(", ");
        }
        if (context != null) {
            builder.append("context=");
            builder.append(context);
        }
        return builder.toString();
    }

    public Integer getLineId() {
        return lineId;
    }

    public Integer getExtensionId() {
        return extensionId;
    }

    public void setLineId(Integer id) {
        lineId = id;
    }

    public void setExtensionId(Integer id) {
        extensionId = id;
    }

    public Integer getEndpointId() { return endpointId; }

    public void setEndpointId(Integer id) { endpointId = id; }

    public void setContext(String context) {
        this.context = context;
    }

    public String getContext() {
        return context;
    }

    public Integer getposition() {
        return position;
    }

    public void setposition(Integer position) {
        this.position = position;
    }

    public String getprovisioningCode() {
        return provisioningCode;
    }

    public void setprovisioningCode(String exten) {
        this.provisioningCode = exten;
    }

    public RestLine getRestLine() {
        return new RestLine(lineId, context, provisioningCode, position, deviceId);
    }

    public void update(RestLine restLine) {
        lineId = restLine.id;
        provisioningCode = restLine.provisioning_code;
        context = restLine.context;
        position = restLine.position;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(o, this);
    }

    public static class RestLine {
        public Integer id;
        public String context;
        public String provisioning_code;
        public Integer position;
        public String device_id;

        public RestLine(Integer id, String context, String provisioning_code, Integer position, String device_id) {
            this.id = id;
            this.context = context;
            this.provisioning_code = provisioning_code;
            this.position = position;
            this.device_id = device_id;
        }
    }

    public static class RestExtension {
        public Integer id;
        public String context;
        public String exten;
    }

    public static class RestSIPEndpoint {
        public Integer id;
        public String name;
    }

}
