package xivo.restapi.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestUser {

    @Test
    public void testToString() {
        User user = new User();
        user.setFirstname("John");
        user.setLastname("Doe");
        user.setEmail("john@doe.com");
        String expectedResult = "firstname=John, lastname=Doe, caller_id=\"John Doe\", email=john@doe.com, ";
        assertEquals(expectedResult, user.toString());
    }

    @Test
    public void testSetFirstname_fullCallerid() {
        User user = new User();
        user.setLastname("Doe");
        user.setFirstname("John");

        assertEquals("\"John Doe\"", user.getCallerid());
    }

    @Test
    public void testSetFirstname_noLastname() {
        User user = new User();
        user.setLastname(null);
        user.setFirstname("John");

        assertEquals("\"John \"", user.getCallerid());
    }

    @Test
    public void testSetLastname_fullCallerid() {
        User user = new User();
        user.setFirstname("John");
        user.setLastname("Doe");

        assertEquals("\"John Doe\"", user.getCallerid());
    }

    @Test
    public void testSetLastname_noFirstname() {
        User user = new User();
        user.setFirstname(null);
        user.setLastname("Doe");

        assertEquals("\" Doe\"", user.getCallerid());
    }

    @Test
    public void testSetNameWithoutCalledIdUpdate() {
        User user = new User();
        user.setFirstname("Foo");
        user.setLastname("Bar");

        user.setNameWithoutCalledIdUpdate("Isaac", "Newton");

        assertEquals("Isaac", user.getFirstname());
        assertEquals("Newton", user.getLastname());
        assertEquals("\"Foo Bar\"", user.getCallerid());
    }

    @Test
    public void testStandardizeCallerid() {
        User u = new User();

        u.setCallerid("Jean Dupond");
        u.standardizeCallerid();
        assertEquals("\"Jean Dupond\"", u.getCallerid());

        u.standardizeCallerid();
        assertEquals("\"Jean Dupond\"", u.getCallerid());

        u.setCallerid("\"Jean Dupond\" <1000>");
        u.standardizeCallerid();
        assertEquals("\"Jean Dupond\" <1000>", u.getCallerid());
    }

    @Test
    public void testSetEmail() {
        User user = new User();
        user.setLastname("Doe");
        user.setFirstname("John");
        user.setEmail("john@doe.com");

        assertEquals("john@doe.com", user.getEmail());
    }

    @Test
    public void testSetVoiceMailLanguage() {
        User user = new User();
        user.setLastname("Doe");
        user.setFirstname("John");
        user.setEmail("john@doe.com");
        user.setVoicemail(new Voicemail());
        user.getVoicemail().setName("John DOE MEVO");
        user.getVoicemail().setNumber("44205");
        user.getVoicemail().setPassword("12345");
        user.getVoicemail().setEmail(user.getEmail());

        assertEquals(null, user.getVoicemail().getLanguage());

        user.getVoicemail().setLanguage("de_DE");
        assertEquals("de_DE", user.getVoicemail().getLanguage());
    }

    @Test
    public void testStandardizeCallerid_nullCallerid() {
        new User().standardizeCallerid();
    }

}
