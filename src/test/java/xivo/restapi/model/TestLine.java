package xivo.restapi.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import xivo.restapi.model.Line.RestExtension;
import xivo.restapi.model.Line.RestLine;

public class TestLine {

    @Test
    public void testGetRestLine() {
        Line line = new Line("1000");
        line.setContext("internal");
        line.setLineId(3);
        line.setprovisioningCode("456987");
        line.setposition(45);
        line.setDeviceId("deviceId1");

        RestLine restLine = line.getRestLine();

        assertEquals("internal", restLine.context);
        assertEquals("456987", restLine.provisioning_code);
        assertEquals(3, restLine.id.intValue());
        assertEquals(45, restLine.position.intValue());
        assertEquals("deviceId1", restLine.device_id);
    }

    @Test
    public void testUpdate() {
        Line line = new Line("1000");
        RestLine restLine = new RestLine(47, "strange", "789456", 12, "asdf43");

        line.update(restLine);

        assertEquals("strange", line.getContext());
        assertEquals(47, line.getLineId().intValue());
        assertEquals("789456", line.getprovisioningCode());
        assertEquals(12, line.getposition().intValue());
    }

    @Test
    public void testConstructor() {
        String provisioning = "116987";
        RestLine rl = new RestLine(1, "internal", provisioning, 1, "asdf43");
        RestExtension re = new RestExtension();
        re.id = 2;
        re.context = "internal";
        re.exten = "2000";
        Links.LineEndpoint le = new Links.LineEndpoint();
        le.endpoint_id = 12;


        Line line = new Line(rl, re, le);

        assertEquals("2000", line.getNumber());
        assertEquals("internal", line.getContext());
        assertEquals(1, line.getLineId().intValue());
        assertEquals(2, line.getExtensionId().intValue());
        assertEquals(12, line.getEndpointId().intValue());
        assertEquals(1, line.getposition().intValue());
        assertEquals(provisioning, line.getprovisioningCode());
        assertEquals("asdf43", line.getDeviceId());
    }
}
