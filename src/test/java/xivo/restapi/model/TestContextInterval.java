package xivo.restapi.model;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestContextInterval {

    @Test
    public void testConstructor_nullDidLength() {
        ContextInterval it = new ContextInterval("1000", "1099", 0);
        assertEquals(4, it.didLength);
        assertEquals("", it.prefix);
    }

    @Test
    public void testConstructor_extractPrefix() {
        ContextInterval it = new ContextInterval("0230210090", "0230210099", 4);
        assertEquals(4, it.didLength);
        assertEquals("023021", it.prefix);
    }

    @Test
    public void testIsNumberInbound_true() {
        assertTrue(new ContextInterval("5000", "5099").isNumberInbound("5000"));
        assertTrue(new ContextInterval("5000", "5099").isNumberInbound("5099"));
        assertTrue(new ContextInterval("5000", "5099").isNumberInbound("5010"));
        assertTrue(new ContextInterval("5000", null).isNumberInbound("5010"));
    }

    @Test
    public void testIsNumberInbound_false() {
        assertFalse(new ContextInterval("5000", "5099").isNumberInbound("4099"));
        assertFalse(new ContextInterval("5000", "5099").isNumberInbound("5100"));
        assertFalse(new ContextInterval("5000", null).isNumberInbound("4099"));
    }
}
