package xivo.restapi.connection.managers;

import java.util.HashMap;
import java.util.UUID;

import junit.framework.TestCase;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.model.Info;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ConfigurationManager.class })
public class TestConfigurationManager extends TestCase {

    private RequestExecutor executor;
    private String configurationUrl;
    private String baseUrl;
    private ConfigurationManager manager;
    private JsonSerializer serializer;
    private String response = "";
    private String serializedString = "";

    @Before
    public void setUp() {
        executor = PowerMock.createMock(RequestExecutor.class);
        configurationUrl = "configuration";
        baseUrl = "host";
        serializer = PowerMock.createNiceMock(JsonSerializer.class);

        manager = new ConfigurationManager(executor, serializer, baseUrl, configurationUrl);
    }

    @Test
    public void testDisableLiveReload() throws Exception {
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, baseUrl + configurationUrl + "/live_reload").andReturn(putMock);
        @SuppressWarnings("unchecked")
        HashMap<String, Boolean> mapMock = PowerMock.createNiceMock(HashMap.class);
        PowerMock.expectNew(HashMap.class).andReturn(mapMock);
        EasyMock.expect(mapMock.put("enabled", false)).andReturn(null);
        EasyMock.expect(serializer.serializePut(mapMock)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(response);
        PowerMock.replayAll();

        manager.disableLiveReload();

        PowerMock.verifyAll();
    }

    @Test
    public void testEnableLiveReload() throws Exception {
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, baseUrl + configurationUrl + "/live_reload").andReturn(putMock);
        @SuppressWarnings("unchecked")
        HashMap<String, Boolean> mapMock = PowerMock.createNiceMock(HashMap.class);
        PowerMock.expectNew(HashMap.class).andReturn(mapMock);
        EasyMock.expect(mapMock.put("enabled", true)).andReturn(null);
        EasyMock.expect(serializer.serializePut(mapMock)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(response);
        PowerMock.replayAll();

        manager.enableLiveReload();

        PowerMock.verifyAll();
    }

    @Test
    public void testGetInfo() throws Exception {
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        response = "{\"uuid\": \"df485e77-c187-442d-94e5-51ddd618f129\"}";
        PowerMock.expectNew(HttpGet.class, baseUrl + "/infos").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, Info.class)).andReturn(new Info("df485e77-c187-442d-94e5-51ddd618f129"));
        PowerMock.replayAll();

        Info res = manager.getInfo();
        assertEquals("df485e77-c187-442d-94e5-51ddd618f129", res.getUuid().toString());
        PowerMock.verifyAll();
    }
}
