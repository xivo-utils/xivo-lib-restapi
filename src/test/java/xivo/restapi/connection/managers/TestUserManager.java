package xivo.restapi.connection.managers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.CtiConfiguration;
import xivo.restapi.model.Line;
import xivo.restapi.model.Links.UserCti;
import xivo.restapi.model.Links.UserLine;
import xivo.restapi.model.Links.UserVoicemail;
import xivo.restapi.model.User;
import xivo.restapi.model.Voicemail;

import com.google.gson.reflect.TypeToken;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ UserManager.class })
public class TestUserManager {

    private RequestExecutor executor;
    private JsonSerializer serializer;
    private String usersUrl;
    private UserManager manager;
    private User user;
    private String response = "";
    private String serializedString = "";

    @Before
    public void setUp() {
        executor = PowerMock.createMock(RequestExecutor.class);
        serializer = PowerMock.createMock(JsonSerializer.class);
        usersUrl = "http://localhost:50050/1.1/users";
        manager = new UserManager(executor, serializer, usersUrl);
        prepareUser();
    }

    private void prepareUser() {
        user = new User();
        user.setFirstname("Marc");
        user.setId(4);
        Voicemail voicemail = new Voicemail();
        voicemail.setId(2);
        user.setVoicemail(voicemail);
        Line userLine = new Line();
        userLine.setLineId(2);
        userLine.setExtensionId(3);
        user.setLine(userLine);
    }

    @Test
    public void testUpdate() throws Exception {
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, usersUrl + "/4").andReturn(putMock);
        EasyMock.expect(serializer.serializePut(user)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(response);
        PowerMock.replayAll();

        manager.update(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testUpdate_notNullPassword() throws Exception {
        user.setPassword("test");
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, usersUrl + "/4").andReturn(putMock);
        EasyMock.expect(serializer.serializePut(user)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(response);
        PowerMock.replayAll();

        manager.update(user);

        PowerMock.verifyAll();
    }

    @Test(expected = WebServicesException.class)
    public void testUpdateFail() throws Exception {
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, usersUrl + "/4").andReturn(putMock);
        EasyMock.expect(serializer.serializePut(user)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andThrow(
                new RequestException("my error message", 400));
        PowerMock.replayAll();

        manager.update(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testUpdateWithEntityId() throws Exception {
        user.entityId = 4;
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, usersUrl + "/4").andReturn(putMock);
        EasyMock.expect(serializer.serializePut(user)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(response);
        PowerMock.expectNew(HttpPut.class, usersUrl + "/"+user.getId() + "/entities/4").andReturn(putMock);
        EasyMock.expect(executor.executeRequest(putMock)).andReturn(response);
        PowerMock.replayAll();

        manager.update(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testCreate() throws Exception {
        user = new User();
        response = "{\"id\": 41,\"links\": [{\"rel\": \"users\", \"href\": \"http://...\"}]}";

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, usersUrl).andReturn(postMock);
        EasyMock.expect(serializer.serializePost(user)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response);
        EasyMock.expect(serializer.extractId(response)).andReturn(41);
        PowerMock.replayAll();

        User createdUser = manager.create(user);

        PowerMock.verifyAll();
        assertEquals(41, createdUser.getId().intValue());
    }

    @Test
    public void testCreateWithEntityId() throws Exception {
        user = new User();
        user.entityId = 3;
        response = "{\"id\": 41,\"links\": [{\"rel\": \"users\", \"href\": \"http://...\"}]}";

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, usersUrl).andReturn(postMock);
        EasyMock.expect(serializer.serializePost(user)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response);
        EasyMock.expect(serializer.extractId(response)).andReturn(41);
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, usersUrl + "/41/entities/3").andReturn(putMock);
        EasyMock.expect(executor.executeRequest(putMock)).andReturn(response);
        PowerMock.replayAll();

        User createdUser = manager.create(user);

        PowerMock.verifyAll();
        assertEquals(41, createdUser.getId().intValue());
    }

    @Test(expected = WebServicesException.class)
    public void testCreateFail() throws Exception {
        user = new User();

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, usersUrl).andReturn(postMock);
        EasyMock.expect(serializer.serializePost(user)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andThrow(
                new RequestException("my error message", 400));
        PowerMock.replayAll();

        manager.create(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testList() throws Exception {
        List<User> users = Arrays.asList(new User[] { user });
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, usersUrl).andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeUsers(response)).andReturn(users);
        PowerMock.replayAll();

        List<User> result = manager.list();

        PowerMock.verifyAll();
        assertEquals(users, result);
    }

    @Test(expected = WebServicesException.class)
    public void testListError() throws Exception {
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, usersUrl).andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andThrow(new RequestException("my error message", 500));
        PowerMock.replayAll();

        manager.list();

        PowerMock.verifyAll();
    }

    @Test
    public void testGet() throws Exception {
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, usersUrl + "/" + user.getId()).andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, User.class)).andReturn(user);
        PowerMock.replayAll();

        User result = manager.get(user.getId());

        PowerMock.verifyAll();
        assertSame(user, result);
    }

    @Test
    public void testAssociateVoicemail() throws Exception {
        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, usersUrl + "/4/voicemail").andReturn(postMock);
        @SuppressWarnings("unchecked")
        HashMap<String, Object> mapMock = PowerMock.createNiceMock(HashMap.class);
        PowerMock.expectNew(HashMap.class).andReturn(mapMock);
        EasyMock.expect(mapMock.put("voicemail_id", 2)).andReturn(null);
        EasyMock.expect(serializer.serializePost(mapMock)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response);

        PowerMock.replayAll();

        manager.associateVoicemail(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testAssociateLine() throws Exception {
        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, usersUrl + "/4/lines").andReturn(postMock);
        @SuppressWarnings("unchecked")
        HashMap<String, Integer> mapMock = PowerMock.createNiceMock(HashMap.class);
        PowerMock.expectNew(HashMap.class).andReturn(mapMock);
        EasyMock.expect(mapMock.put("line_id", 2)).andReturn(null);
        EasyMock.expect(serializer.serializePost(mapMock)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response);
        PowerMock.replayAll();

        manager.associateLine(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testDissociateLine() throws Exception {
        HttpDelete deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, usersUrl + "/" + user.getId() + "/lines/" + user.getLine().getLineId())
                .andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn(response);
        PowerMock.replayAll();

        manager.dissociateLine(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testDelete() throws Exception {
        HttpDelete deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, usersUrl + "/" + user.getId()).andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn(response);
        PowerMock.replayAll();

        manager.delete(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testDissociateVoicemail() throws Exception {
        HttpDelete deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, usersUrl + "/" + user.getId() + "/voicemail").andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn(response);
        PowerMock.replayAll();

        manager.dissociateVoicemail(user);

        PowerMock.verifyAll();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetUserLines() throws Exception {
        UserLine userLine = new UserLine();
        userLine.user_id = 5;
        userLine.line_id = 3;
        List<UserLine> userLines = new LinkedList<UserLine>();
        userLines.add(userLine);
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, usersUrl + "/5/lines").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn("");
        EasyMock.expect(serializer.deserializeList(EasyMock.eq(""), EasyMock.notNull(TypeToken.class))).andReturn(
                userLines);
        PowerMock.replayAll();

        List<UserLine> result = manager.getUserLines(5);

        PowerMock.verifyAll();
        assertSame(result, userLines);
    }

    @Test
    public void testGetUserVoicemail() throws Exception {
        UserVoicemail uv = new UserVoicemail();
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, usersUrl + "/5/voicemail").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn("");
        EasyMock.expect(serializer.deserializeObject("", UserVoicemail.class)).andReturn(uv);
        PowerMock.replayAll();

        UserVoicemail result = manager.getUserVoicemail(5);

        PowerMock.verifyAll();
        assertSame(result, uv);
    }

    @Test
    public void testGetUserVoicemailNotFound() throws Exception {
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        RequestException exc = new RequestException("my error message", 404);
        PowerMock.expectNew(HttpGet.class, usersUrl + "/5/voicemail").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andThrow(exc);
        PowerMock.replayAll();

        UserVoicemail result = manager.getUserVoicemail(5);

        PowerMock.verifyAll();
        assertNull(result);
    }

    @Test
    public void testGetUserCti() throws Exception {
        UserCti ucp = new UserCti();
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, usersUrl + "/5/cti").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn("");
        EasyMock.expect(serializer.deserializeObject("", UserCti.class)).andReturn(ucp);
        PowerMock.replayAll();

        UserCti result = manager.getUserCti(5);

        PowerMock.verifyAll();
        assertSame(result, ucp);
    }

    @Test
    public void testUpdateCtiConfiguration() throws Exception {
        CtiConfiguration ctiConfig = new CtiConfiguration(true, null);
        user.setCtiConfiguration(ctiConfig);
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, usersUrl + "/" + user.getId() + "/cti").andReturn(putMock);
        EasyMock.expect(serializer.serializePut(EasyMock.notNull(UserCti.class))).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(response);

        PowerMock.replayAll();

        manager.updateCtiConfiguration(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testUpdateCtiConfigurationNullConfig() throws Exception {
        user.setCtiConfiguration(null);

        manager.updateCtiConfiguration(user);
    }

    @Test
    public void testCountUsers() throws Exception {
        response = "{\"total\": 41,\"items\": []}";

        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, usersUrl + "?limit=1").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.extractTotal(response)).andReturn(41);
        PowerMock.replayAll();

        int res = manager.countUsers();

        PowerMock.verifyAll();
        assertEquals(41, res);
    }

    @Test
    public void testSetUserEntity() throws Exception {
        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        user.entityId = 3;
        PowerMock.expectNew(HttpPut.class, usersUrl + "/" + user.getId() + "/entities/" + user.entityId).andReturn(putMock);
        EasyMock.expect(executor.executeRequest(putMock)).andReturn(response);

        PowerMock.replayAll();

        manager.setUserEntity(user);

        PowerMock.verifyAll();
    }

}
