package xivo.restapi.connection.managers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.connection.WebServicesException;
import xivo.restapi.model.Line;
import xivo.restapi.model.Line.RestExtension;
import xivo.restapi.model.Line.RestLine;
import xivo.restapi.model.Links.LineEndpoint;
import xivo.restapi.model.Links.LineExtension;
import xivo.restapi.model.User;

import com.google.gson.reflect.TypeToken;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LineManager.class })
public class TestLineManager {

    private RequestExecutor executor;
    private JsonSerializer serializer;
    private String linesUrl;
    private String extensionsUrl;
    private String endpointsUrl;
    private LineManager manager, manager2;
    private Line line;

    private String customIncall = "from-extern";


    @Before
    public void setUp() {
        executor = PowerMock.createMock(RequestExecutor.class);
        serializer = PowerMock.createMock(JsonSerializer.class);
        linesUrl = "lines";
        extensionsUrl = "extensions";
        endpointsUrl = "endpoints";
        manager = new LineManager(executor, serializer, linesUrl, extensionsUrl, endpointsUrl);
        manager2 = new LineManager(executor, serializer, linesUrl, extensionsUrl, endpointsUrl, customIncall);
        line = new Line();
        line.setContext("default");
        line.setposition(1);
        line.setNumber("1234");
        line.setExtensionId(2);
        line.setLineId(3);
        line.setName("lineName");
        line.setEndpointId(9);
    }

    @Test
    public void testCreate() throws Exception {
        String response1 = "{\"id\": 3, \"provisioning_code\": \"123456\", \"links\": []}";
        String response2 = "{\"id\": 42, \"links\": []}";
        String genericResponse = "response";
        String serializedString = "serialization result";
        Line.RestSIPEndpoint endpoint = new Line.RestSIPEndpoint();
        endpoint.id = line.getEndpointId();
        endpoint.name = "endpointName";
        LineEndpoint le = new LineEndpoint();
        le.endpoint_id = line.getEndpointId();
        le.line_id = line.getLineId();

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, linesUrl).andReturn(postMock);
        EasyMock.expect(serializer.serializePost(EasyMock.isA(RestLine.class))).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response1);
        EasyMock.expect(serializer.deserializeObject(response1, RestLine.class)).andReturn(
                new RestLine(3, "default", "123456", 1, "asdfa4"));

        postMock = PowerMock.createNiceMock(HttpPost.class);
        Map<String, Object> expectedMap = new HashMap<String, Object>();
        expectedMap.put("exten", "1234");
        expectedMap.put("context", "default");
        PowerMock.expectNew(HttpPost.class, extensionsUrl).andReturn(postMock);
        EasyMock.expect(serializer.serializePost(expectedMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response2);
        EasyMock.expect(serializer.extractId(response2)).andReturn(42);

        PowerMock.expectNew(HttpPost.class, endpointsUrl).andReturn(postMock);
        Map<String, Object> endpointMap = new HashMap<String, Object>();
        endpointMap.put("name", line.getName());

        EasyMock.expect(serializer.serializePost(endpointMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(genericResponse);
        EasyMock.expect(serializer.deserializeObject(genericResponse, Line.RestSIPEndpoint.class)).andReturn(endpoint);

        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, linesUrl + "/" + line.getLineId() + "/endpoints/sip/" + le.endpoint_id).andReturn(putMock);
        Map<String, Object> lineEndpointMap = new HashMap<String, Object>();
        lineEndpointMap.put("line_id", line.getLineId());
        lineEndpointMap.put("endpoint_id", line.getEndpointId());
        EasyMock.expect(serializer.serializePost(lineEndpointMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(genericResponse);

        postMock = PowerMock.createNiceMock(HttpPost.class);
        Map<String, Object> otherMap = new HashMap<String, Object>();
        otherMap.put("extension_id", 42);
        PowerMock.expectNew(HttpPost.class, linesUrl + "/" + line.getLineId() + "/extension").andReturn(postMock);
        EasyMock.expect(serializer.serializePost(otherMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response2);

        PowerMock.replayAll();

        manager.create(line);

        PowerMock.verifyAll();
        assertEquals(3, line.getLineId().intValue());
        assertEquals("123456", line.getprovisioningCode());
        assertEquals(42, line.getExtensionId().intValue());
        assertEquals(9, line.getEndpointId().intValue());
        assertEquals("endpointName", line.getName());
    }

    @Test
    public void testCreateFailsOnExtension() throws Exception {
        String response1 = "{\"id\": 41, \"provisioning_code\": 123456, \"links\": []}";
        String serializedString = "serialization result";
        Line.RestSIPEndpoint endpoint = new Line.RestSIPEndpoint();
        endpoint.id = line.getEndpointId();
        endpoint.name = "endpointName";
        LineEndpoint le = new LineEndpoint();
        le.endpoint_id = line.getEndpointId();
        le.line_id = line.getLineId();

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, linesUrl).andReturn(postMock);
        EasyMock.expect(serializer.serializePost(EasyMock.isA(RestLine.class))).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response1);
        EasyMock.expect(serializer.deserializeObject(response1, RestLine.class)).andReturn(
                new RestLine(41, "default", "123456", 1, "asdf23"));

        postMock = PowerMock.createNiceMock(HttpPost.class);
        Map<String, Object> expectedMap = new HashMap<String, Object>();
        expectedMap.put("exten", "1234");
        expectedMap.put("context", "default");
        PowerMock.expectNew(HttpPost.class, extensionsUrl).andReturn(postMock);
        EasyMock.expect(serializer.serializePost(expectedMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andThrow(
                new RequestException("my error message", 400));

        HttpDelete deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, linesUrl + "/41").andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn("");

        PowerMock.replayAll();

        try {
            manager.create(line);
            fail("WebServicesException not raised.");
        } catch (WebServicesException e) {
            PowerMock.verifyAll();
        }
    }

    @Test
    public void testCreateFailsOnAssociation() throws Exception {
        String response1 = "{\"id\": 3, \"provisioning_code\": 123456, \"links\": []}";
        String response2 = "{\"id\": 42, \"links\": []}";
        String serializedString = "serialization result";
        String genericResponse = "generic response";
        Line.RestSIPEndpoint endpoint = new Line.RestSIPEndpoint();
        endpoint.id = line.getEndpointId();
        endpoint.name = "endpointName";
        LineEndpoint le = new LineEndpoint();
        le.endpoint_id = line.getEndpointId();
        le.line_id = line.getLineId();

        HttpPost postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, linesUrl).andReturn(postMock);
        EasyMock.expect(serializer.serializePost(EasyMock.isA(RestLine.class))).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response1);
        EasyMock.expect(serializer.deserializeObject(response1, RestLine.class)).andReturn(
                new RestLine(3, "default", "123456", 1, "345asd"));

        postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, extensionsUrl).andReturn(postMock);
        Map<String, Object> expectedMap = new HashMap<String, Object>();
        expectedMap.put("exten", "1234");
        expectedMap.put("context", "default");
        EasyMock.expect(serializer.serializePost(expectedMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(response2);
        EasyMock.expect(serializer.extractId(response2)).andReturn(42);

        PowerMock.expectNew(HttpPost.class, endpointsUrl).andReturn(postMock);
        Map<String, Object> endpointMap = new HashMap<String, Object>();
        endpointMap.put("name", line.getName());

        EasyMock.expect(serializer.serializePost(endpointMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andReturn(genericResponse);
        EasyMock.expect(serializer.deserializeObject(genericResponse, Line.RestSIPEndpoint.class)).andReturn(endpoint);

        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, linesUrl + "/" + line.getLineId() + "/endpoints/sip/" + le.endpoint_id).andReturn(putMock);
        Map<String, Object> lineEndpointMap = new HashMap<String, Object>();
        lineEndpointMap.put("line_id", line.getLineId());
        lineEndpointMap.put("endpoint_id", line.getEndpointId());
        EasyMock.expect(serializer.serializePost(lineEndpointMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(genericResponse);

        Map<String, Object> otherMap = new HashMap<String, Object>();
        otherMap.put("extension_id", 42);
        postMock = PowerMock.createNiceMock(HttpPost.class);
        PowerMock.expectNew(HttpPost.class, linesUrl + "/" + line.getLineId() + "/extension").andReturn(postMock);
        EasyMock.expect(serializer.serializePost(otherMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(postMock, serializedString)).andThrow(
                new RequestException("my error message", 400));

        HttpDelete deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, linesUrl + "/" + line.getLineId() + "/endpoints/sip/" + line.getEndpointId()).andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn("");

        deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, endpointsUrl + "/" + line.getEndpointId()).andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn("");

        deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, linesUrl + "/" + line.getLineId()).andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn("");

        deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, extensionsUrl + "/42").andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn("");

        PowerMock.replayAll();

        try {
            manager.create(line);
            fail("WebServicesException not raised.");
        } catch (WebServicesException e) {
            PowerMock.verifyAll();
        }
    }

    @Test
    public void testUpdate() throws Exception {
        String response = "";
        String serializedString = "serialization result";

        HttpPut putMock = PowerMock.createNiceMock(HttpPut.class);
        Map<String, Object> expectedMap = new HashMap<String, Object>();
        expectedMap.put("context", "default");
        expectedMap.put("position", 1);
        PowerMock.expectNew(HttpPut.class, linesUrl + "/3").andReturn(putMock);
        EasyMock.expect(serializer.serializePut(expectedMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(response);

        Map<String, Object> otherMap = new HashMap<String, Object>();
        otherMap.put("exten", "1234");
        otherMap.put("context", "default");
        putMock = PowerMock.createNiceMock(HttpPut.class);
        PowerMock.expectNew(HttpPut.class, extensionsUrl + "/2").andReturn(putMock);
        EasyMock.expect(serializer.serializePut(otherMap)).andReturn(serializedString);
        EasyMock.expect(executor.fillAndExecuteRequest(putMock, serializedString)).andReturn(response);

        PowerMock.replayAll();

        manager.update(line);

        PowerMock.verifyAll();
    }

    @Test
    public void testDelete() throws Exception {
        String response = "";

        HttpDelete deleteMock = PowerMock.createNiceMock(HttpDelete.class);
        PowerMock.expectNew(HttpDelete.class, linesUrl + "/3/extension").andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn(response);
        PowerMock.expectNew(HttpDelete.class, linesUrl + "/3/endpoints/sip/" + line.getEndpointId()).andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn(response);
        PowerMock.expectNew(HttpDelete.class, endpointsUrl + "/" + line.getEndpointId()).andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn(response);
        PowerMock.expectNew(HttpDelete.class, linesUrl + "/3").andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn(response);
        PowerMock.expectNew(HttpDelete.class, extensionsUrl + "/2").andReturn(deleteMock);
        EasyMock.expect(executor.executeRequest(deleteMock)).andReturn(response);
        PowerMock.replayAll();

        manager.delete(line);

        PowerMock.verifyAll();
    }

    @Test
    public void testGet() throws Exception {
        RestLine restLine = new RestLine(3, "default", "123456", null, null);
        LineExtension lineExten = new LineExtension();
        lineExten.extension_id = 4;
        lineExten.line_id = 3;
        RestExtension restExtension = new RestExtension();
        restExtension.exten = "1234";
        restExtension.context = "default";
        restExtension.id = 4;
        String response = "response1";
        LineEndpoint lineEndpoint = new LineEndpoint();
        lineEndpoint.endpoint_id = 9;
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, linesUrl + "/3").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, RestLine.class)).andReturn(restLine);
        PowerMock.expectNew(HttpGet.class, linesUrl + "/3/extension").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, LineExtension.class)).andReturn(lineExten);
        PowerMock.expectNew(HttpGet.class, extensionsUrl + "/4").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, RestExtension.class)).andReturn(restExtension);
        PowerMock.expectNew(HttpGet.class, linesUrl + "/3/endpoints/sip").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, LineEndpoint.class)).andReturn(lineEndpoint);
        PowerMock.replayAll();

        Line result = manager.get(3);

        PowerMock.verifyAll();
        assertEquals("1234", result.getNumber());
        assertEquals("default", result.getContext());
        assertEquals(3, result.getLineId().intValue());
        assertEquals(4, result.getExtensionId().intValue());
        assertEquals(9, result.getEndpointId().intValue());
    }

    @Test
    public void testSetLineAndIncall() throws Exception {
        int lineId = 3;
        RestLine restLine = new RestLine(3, "default", "123456", null, null);
        LineExtension lineExten1 = new LineExtension();
        lineExten1.extension_id = 4;
        lineExten1.line_id = lineId;
        LineExtension lineExten2 = new LineExtension();
        lineExten2.extension_id = 5;
        lineExten2.line_id = lineId;
        RestExtension restExtension1 = new RestExtension();
        restExtension1.exten = "1234";
        restExtension1.context = "default";
        restExtension1.id = 4;
        RestExtension restExtension2 = new RestExtension();
        restExtension2.exten = "5000";
        restExtension2.context = LineManager.EXTERNAL_CONTEXT+"3rd";
        restExtension2.id = 5;
        LineEndpoint lineEndpoint = new LineEndpoint();
        lineEndpoint.endpoint_id = 9;

        String response = "response1";
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, linesUrl + "/3").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, RestLine.class)).andReturn(restLine);
        PowerMock.expectNew(HttpGet.class, linesUrl + "/3/extensions").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeList(EasyMock.eq(response), EasyMock.notNull(TypeToken.class)))
                .andReturn(Arrays.asList(new LineExtension[] { lineExten1, lineExten2 }));
        PowerMock.expectNew(HttpGet.class, linesUrl + "/3/endpoints/sip").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, LineEndpoint.class)).andReturn(lineEndpoint);
        PowerMock.expectNew(HttpGet.class, extensionsUrl + "/4").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, RestExtension.class)).andReturn(restExtension1);
        PowerMock.expectNew(HttpGet.class, extensionsUrl + "/5").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, RestExtension.class)).andReturn(restExtension2);
        PowerMock.replayAll();

        User user = new User();
        manager.setLineAndIncall(user, 3);

        PowerMock.verifyAll();
        assertEquals("1234", user.getLine().getNumber());
        assertEquals("default", user.getLine().getContext());
        assertEquals(3, user.getLine().getLineId().intValue());
        assertEquals(4, user.getLine().getExtensionId().intValue());
        assertEquals(9, user.getLine().getEndpointId().intValue());
        assertEquals("5000", user.getIncomingCall().getSda());
        assertEquals(LineManager.EXTERNAL_CONTEXT+"3rd", user.getIncomingCall().getContext());
        assertEquals(new Integer(5), user.getIncomingCall().getExtensionId());
    }

    @Test
    public void testSetLineAndCustomIncall() throws Exception {
        int lineId = 3;
        RestLine restLine = new RestLine(3, "default", "123456", null, null);
        LineExtension lineExten1 = new LineExtension();
        lineExten1.extension_id = 4;
        lineExten1.line_id = lineId;
        LineExtension lineExten2 = new LineExtension();
        lineExten2.extension_id = 5;
        lineExten2.line_id = lineId;
        RestExtension restExtension1 = new RestExtension();
        restExtension1.exten = "1234";
        restExtension1.context = "default";
        restExtension1.id = 4;
        RestExtension restExtension2 = new RestExtension();
        restExtension2.exten = "5000";
        restExtension2.context = customIncall + "3rd";
        restExtension2.id = 5;
        LineEndpoint lineEndpoint = new LineEndpoint();
        lineEndpoint.endpoint_id = 9;

        String response = "response1";
        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, linesUrl + "/3").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, RestLine.class)).andReturn(restLine);
        PowerMock.expectNew(HttpGet.class, linesUrl + "/3/extensions").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeList(EasyMock.eq(response), EasyMock.notNull(TypeToken.class)))
                .andReturn(Arrays.asList(new LineExtension[] { lineExten1, lineExten2 }));
        PowerMock.expectNew(HttpGet.class, linesUrl + "/3/endpoints/sip").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, LineEndpoint.class)).andReturn(lineEndpoint);
        PowerMock.expectNew(HttpGet.class, extensionsUrl + "/4").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, RestExtension.class)).andReturn(restExtension1);
        PowerMock.expectNew(HttpGet.class, extensionsUrl + "/5").andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        EasyMock.expect(serializer.deserializeObject(response, RestExtension.class)).andReturn(restExtension2);
        PowerMock.replayAll();

        User user = new User();
        manager2.setLineAndIncall(user, 3);

        PowerMock.verifyAll();
        assertEquals("1234", user.getLine().getNumber());
        assertEquals("default", user.getLine().getContext());
        assertEquals(3, user.getLine().getLineId().intValue());
        assertEquals(4, user.getLine().getExtensionId().intValue());
        assertEquals("5000", user.getIncomingCall().getSda());
        assertEquals(customIncall + "3rd", user.getIncomingCall().getContext());
        assertEquals(new Integer(5), user.getIncomingCall().getExtensionId());
    }

}
