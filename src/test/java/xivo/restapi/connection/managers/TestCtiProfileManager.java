package xivo.restapi.connection.managers;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.apache.http.client.methods.HttpGet;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import xivo.restapi.connection.JsonSerializer;
import xivo.restapi.connection.RequestExecutor;
import xivo.restapi.model.CtiProfile;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CtiProfileManager.class })
public class TestCtiProfileManager extends TestCase {

    private RequestExecutor executor;
    private String ctiProfilesUrl;
    private CtiProfileManager manager;
    private CtiProfile ctiProfile;
    private String response = "";

    @Before
    public void setUp() {
        executor = PowerMock.createMock(RequestExecutor.class);
        ctiProfilesUrl = "cti_profiles";
        manager = new CtiProfileManager(executor, new JsonSerializer(), ctiProfilesUrl);
        ctiProfile = new CtiProfile();
        ctiProfile.setId(6);
    }

    @Test
    public void testList() throws Exception {
        response = "{\"total\": 2,\n" +
                "\"items\": [\n" +
                "{\"id\": 1, \"name\": \"Client\"},\n" +
                "{\"id\": 2, \"name\": \"Agent\"}\n" +
                "]}";

        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, ctiProfilesUrl).andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        PowerMock.replayAll();

        List<CtiProfile> result = manager.list();

        assertEquals(1, result.get(0).getId().intValue());
        assertEquals("Client", result.get(0).getName());
        assertEquals(2, result.get(1).getId().intValue());
        assertEquals("Agent", result.get(1).getName());
        PowerMock.verifyAll();
    }

    @Test
    public void testGetCtiProfileByName() throws Exception {
        response = "{\"total\": 2,\n" +
                "\"items\": [\n" +
                "{\"id\": 1, \"name\": \"Client\"},\n" +
                "{\"id\": 2, \"name\": \"Agent\"}\n" +
                "]}";

        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, ctiProfilesUrl).andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        PowerMock.replayAll();

        CtiProfile result = manager.getCtiProfileByName("Agent");

        assertEquals(2, result.getId().intValue());
        assertEquals("Agent", result.getName());
    }

    @Test
    public void testGetCtiProfileByNameUsingCache() throws Exception {
        CtiProfile profile1 = new CtiProfile();
        profile1.setId(1);
        profile1.setName("Client");
        CtiProfile profile2 = new CtiProfile();
        profile2.setId(2);
        profile2.setName("Agent");
        List<CtiProfile> profiles = new ArrayList<CtiProfile>();
        profiles.add(profile1);
        profiles.add(profile2);
        Whitebox.setInternalState(manager, "cache", profiles);

        CtiProfile result = manager.getCtiProfileByName("Agent");

        assertEquals(2, result.getId().intValue());
        assertEquals("Agent", result.getName());
    }

    @Test
    public void testGetCtiProfileById() throws Exception {
        response = "{\"total\": 2,\n" +
                "\"items\": [\n" +
                "{\"id\": 1, \"name\": \"Client\"},\n" +
                "{\"id\": 2, \"name\": \"Agent\"}\n" +
                "]}";

        HttpGet getMock = PowerMock.createNiceMock(HttpGet.class);
        PowerMock.expectNew(HttpGet.class, ctiProfilesUrl).andReturn(getMock);
        EasyMock.expect(executor.executeRequest(getMock)).andReturn(response);
        PowerMock.replayAll();

        CtiProfile result = manager.getCtiProfileById(1);

        assertEquals(1, result.getId().intValue());
        assertEquals("Client", result.getName());
    }

    @Test
    public void testGetCtiProfileByIdUsingCache() throws Exception {
        CtiProfile profile1 = new CtiProfile();
        profile1.setId(1);
        profile1.setName("Client");
        CtiProfile profile2 = new CtiProfile();
        profile2.setId(2);
        profile2.setName("Agent");
        List<CtiProfile> profiles = new ArrayList<CtiProfile>();
        profiles.add(profile1);
        profiles.add(profile2);
        Whitebox.setInternalState(manager, "cache", profiles);

        CtiProfile result = manager.getCtiProfileById(1);

        assertEquals(1, result.getId().intValue());
        assertEquals("Client", result.getName());
    }
}
