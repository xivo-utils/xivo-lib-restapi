package xivo.restapi.connection;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import junit.framework.TestCase;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import xivo.restapi.connection.RequestException;
import xivo.restapi.connection.RequestExecutor;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultHttpClient.class, RequestExecutor.class, StringEntity.class })
public class TestRequestExecutor extends TestCase {

    private DefaultHttpClient clientMock;
    private HttpResponse response;
    private StatusLine line;
    private HttpEntity entity;
    private RequestExecutor executor;

    @Before
    public void setUp() {
        clientMock = PowerMock.createNiceMock(DefaultHttpClient.class);
        response = PowerMock.createNiceMock(HttpResponse.class);
        line = PowerMock.createNiceMock(StatusLine.class);
        entity = PowerMock.createNiceMock(HttpEntity.class);
        executor = new RequestExecutor(clientMock);
    }

    @Test
    public final void testFillAndExecuteRequest() throws Exception {
        String content = "The content of the request";
        String responseBody = "This is the response";
        RequestExecutor localExecutor = PowerMock.createPartialMock(RequestExecutor.class, "executeRequest");
        HttpPost post = PowerMock.createNiceMock(HttpPost.class);
        StringEntity entity = PowerMock.createNiceMock(StringEntity.class);
        PowerMock.expectNew(StringEntity.class, content, org.apache.http.Consts.UTF_8).andReturn(entity);
        post.setEntity(entity);
        EasyMock.expect(localExecutor.executeRequest(post)).andReturn(responseBody);
        PowerMock.replayAll();

        String result = localExecutor.fillAndExecuteRequest(post, content);

        PowerMock.verifyAll();
        assertEquals(responseBody, result);
    }

    @Test
    public final void testExecuteRequestSuccess() throws IOException, RequestException {
        String responseBody = "Test de réponse";
        HttpGet mockGet = PowerMock.createNiceMock(HttpGet.class);
        mockGet.setHeader("Content-Type", "application/json");
        mockGet.setHeader("Accept", "application/json");
        EasyMock.expect(clientMock.execute(mockGet)).andReturn(response);
        EasyMock.expect(response.getStatusLine()).andReturn(line);
        EasyMock.expect(line.getStatusCode()).andReturn(HttpStatus.SC_OK);
        EasyMock.expect(response.getEntity()).andReturn(entity);
        EasyMock.expect(entity.getContent()).andReturn(new ByteArrayInputStream(responseBody.getBytes()));
        PowerMock.replayAll();

        String result = executor.executeRequest(mockGet);

        PowerMock.verifyAll();
        assertEquals(responseBody, result);
    }

    @Test(expected = RequestException.class)
    public final void testExecuteRequestFailure() throws IOException, RequestException {
        String responseBody = "";
        HttpGet mockGet = PowerMock.createNiceMock(HttpGet.class);
        mockGet.setHeader("Content-Type", "application/json");
        mockGet.setHeader("Accept", "application/json");
        EasyMock.expect(clientMock.execute(mockGet)).andReturn(response);
        EasyMock.expect(response.getStatusLine()).andReturn(line);
        EasyMock.expect(line.getStatusCode()).andReturn(HttpStatus.SC_BAD_REQUEST);
        EasyMock.expect(response.getEntity()).andReturn(entity);
        EasyMock.expect(entity.getContent()).andReturn(new ByteArrayInputStream(responseBody.getBytes()));
        PowerMock.replayAll();

        executor.executeRequest(mockGet);
    }
    
    @Test
    public final void testExecuteRequestNoContent() throws IOException, RequestException {
        HttpGet mockGet = PowerMock.createNiceMock(HttpGet.class);
        mockGet.setHeader("Content-Type", "application/json");
        mockGet.setHeader("Accept", "application/json");
        EasyMock.expect(clientMock.execute(mockGet)).andReturn(response);
        EasyMock.expect(response.getStatusLine()).andReturn(line);
        EasyMock.expect(line.getStatusCode()).andReturn(HttpStatus.SC_NO_CONTENT);
        PowerMock.replayAll();

        executor.executeRequest(mockGet);

        PowerMock.verifyAll();
    }

}
