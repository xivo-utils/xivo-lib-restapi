package xivo.restapi.connection;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.reflect.TypeToken;

import xivo.restapi.connection.JsonSerializer.ResponseContent;
import xivo.restapi.model.User;

public class TestJsonSerializer {

    private JsonSerializer serializer;

    @Before
    public void setUp() {
        serializer = new JsonSerializer();
    }

    @Test
    public final void testSerializePost() {
        User user = new User();
        user.setFirstname("Marc");
        user.setLastname("Dupond");
        user.setCallerid(null);
        user.setId(1);
        user.setRingSeconds(45);
        user.setSimultCalls(12);

        String result = serializer.serializePost(user);

        String expectedResult = "{\"id\":1,\"firstname\":\"Marc\",\"lastname\":\"Dupond\",\"ring_seconds\":45,\"simult_calls\":12}";
        assertEquals(expectedResult, result);
    }

    @Test
    public final void testSerializePut() {
        User user = new User();
        user.setFirstname("Marc");
        user.setLastname("Dupond");
        user.setCallerid(null);
        user.setId(1);

        String result = serializer.serializePut(user);

        String expectedResult = "{\"id\":1,\"firstname\":\"Marc\",\"lastname\":\"Dupond\",\"caller_id\":null,\"username\":null,\"password\":null,"
                + "\"music_on_hold\":null,\"outgoing_caller_id\":null,\"mobile_phone_number\":null,\"email\":null,\"userfield\":null,\"timezone\":null,\"language\":null,"
                + "\"description\":null,\"preprocess_subroutine\":null,\"ring_seconds\":null,\"simult_calls\":null}";
        assertEquals(expectedResult, result);
    }

    @Test
    public final void testSerializeMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("param1", "string");
        map.put("param2", new Integer(3));

        String result = serializer.serializePost(map);

        String expectedResult = "{\"param1\":\"string\",\"param2\":3}";
        assertEquals(expectedResult, result);
    }

    @Test
    public void testDeserializeUsers() throws IOException {
        String input = "{\"items\": [{\"id\":44, \"firstname\": \"John\", \"links\": [{\"href\": \"http://localhost:50050/1.1/users/44\", \"rel\": \"users\"}]}, "
                + "{\"id\":45, \"firstname\": \"Jack\", \"links\": [{\"href\": \"http://localhost:50050/1.1/users/45\", \"rel\": \"users\"}]}]}";
        User user1 = new User(), user2 = new User();
        user1.setId(44);
        user1.setFirstname("John");
        user1.setCallerid(null);
        user2.setId(45);
        user2.setFirstname("Jack");
        user2.setCallerid(null);
        List<User> expectedResult = Arrays.asList(new User[] { user1, user2 });

        List<User> result = serializer.deserializeUsers(input);

        assertEquals(expectedResult.toString(), result.toString());
    }

    @Test
    public final void testExtractId() {
        String input = "{\"id\": 42, \"links\": [{\"rel\": \"users\", \"href\": \"http://...\"}]}";

        int result = serializer.extractId(input);

        assertEquals(42, result);
    }

    @Test
    public void testDeserializeList() throws IOException {
        String input = "{\"items\": [{\"id\":44, \"firstname\": \"John\", \"links\": [{\"href\": \"http://localhost:50050/1.1/users/44\", \"rel\": \"users\"}]}, "
                + "{\"id\":45, \"firstname\": \"Jack\", \"links\": [{\"href\": \"http://localhost:50050/1.1/users/45\", \"rel\": \"users\"}]}]}";
        User user1 = new User(), user2 = new User();
        user1.setId(44);
        user1.setFirstname("John");
        user1.setCallerid(null);
        user2.setId(45);
        user2.setFirstname("Jack");
        user2.setCallerid(null);
        List<User> expectedResult = Arrays.asList(user1, user2);

        List<User> result = serializer.deserializeList(input, new TypeToken<ResponseContent<User>>() {
        });

        assertEquals(expectedResult.toString(), result.toString());
    }

    @Test
    public void testExtractTotal() throws IOException {
        String input = "{\"total\": 2, \"items\": [{\"id\":44, \"firstname\": \"John\", \"links\": [{\"href\": \"http://localhost:50050/1.1/users/44\", \"rel\": \"users\"}]}, "
                + "{\"id\":45, \"firstname\": \"Jack\", \"links\": [{\"href\": \"http://localhost:50050/1.1/users/45\", \"rel\": \"users\"}]}]}";

        int result = serializer.extractTotal(input);

        assertEquals(2, result);
    }
}
