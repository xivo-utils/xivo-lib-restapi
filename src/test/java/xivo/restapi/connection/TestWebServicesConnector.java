package xivo.restapi.connection;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import xivo.restapi.connection.managers.ConfigurationManager;
import xivo.restapi.connection.managers.CtiProfileManager;
import xivo.restapi.connection.managers.DeviceManager;
import xivo.restapi.connection.managers.LineManager;
import xivo.restapi.connection.managers.UserManager;
import xivo.restapi.connection.managers.VoicemailManager;
import xivo.restapi.model.*;
import xivo.restapi.model.Links.UserCti;
import xivo.restapi.model.Links.UserLine;
import xivo.restapi.model.Links.UserVoicemail;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebServicesConnector.class })
public class TestWebServicesConnector extends TestCase {

    private WebServicesConnector wsConnector;
    private UserManager userManager;
    private LineManager lineManager;
    private VoicemailManager voicemailManager;
    private CtiProfileManager ctiProfileManager;
    private ConfigurationManager configManager;
    private User user, user2;
    private Voicemail voicemail;
    private List<Voicemail> voicemails;
    private Line line;
    private CtiProfile ctiProfile;
    private CtiConfiguration ctiConfig;
    private TestConfig config;
    private DeviceManager devManager;

    @Before
    public void setUp() {
        prepareConfig();
        wsConnector = new WebServicesConnector(null, null, config);
        userManager = PowerMock.createMock(UserManager.class);
        lineManager = PowerMock.createNiceMock(LineManager.class);
        voicemailManager = PowerMock.createNiceMock(VoicemailManager.class);
        ctiProfileManager = PowerMock.createNiceMock(CtiProfileManager.class);
        configManager = PowerMock.createNiceMock(ConfigurationManager.class);
        devManager = PowerMock.createMock(DeviceManager.class);
        Whitebox.setInternalState(wsConnector, "userManager", userManager);
        Whitebox.setInternalState(wsConnector, "lineManager", lineManager);
        Whitebox.setInternalState(wsConnector, "voicemailManager", voicemailManager);
        Whitebox.setInternalState(wsConnector, "ctiProfileManager", ctiProfileManager);
        Whitebox.setInternalState(wsConnector, "configManager", configManager);
        Whitebox.setInternalState(wsConnector, "deviceManager", devManager);
        user = new User();
        user.setFirstname("Marc");
        user.setId(5);
        voicemail = new Voicemail();
        user.setVoicemail(voicemail);
        line = new Line();
        ctiProfile = new CtiProfile();
        user2 = new User();
        ctiConfig = PowerMock.createNiceMock(CtiConfiguration.class);
    }

    private void prepareConfig() {
        config = new TestConfig();
        config.set(RestapiConfig.USERS_URL_FIELD, "users");
        config.set(RestapiConfig.VOICEMAILS_URL_FIELD, "voicemail");
        config.set(RestapiConfig.LINES_URL_FIELD, "lines");
        config.set(RestapiConfig.EXTENSIONS_URL_FIELD, "extensions");
        config.set(RestapiConfig.SIP_ENDPOINTS_URL_FIELD, "endpoints/sip");
        config.set(RestapiConfig.DEVICES_URL_FIELD, "devices");
        config.set(RestapiConfig.RESTAPI_URL, "http://localhost:50050");
        config.set(RestapiConfig.EXTERNAL_CONTEXT_NAME, LineManager.EXTERNAL_CONTEXT);
    }

    @Test
    public void testCreateUser() throws Exception {
        User createdUser = new User();
        EasyMock.expect(userManager.create(user)).andReturn(createdUser);
        userManager.updateCtiConfiguration(createdUser);
        PowerMock.replayAll();

        wsConnector.createUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testGetAllUsers() throws Exception {
        List<User> users = Arrays.asList(new User[] { user, user2 });
        wsConnector = PowerMock.createPartialMock(WebServicesConnector.class, "fillUser");
        Whitebox.setInternalState(wsConnector, "userManager", userManager);
        EasyMock.expect(userManager.list()).andReturn(users);
        PowerMock.expectPrivate(wsConnector, "fillUser", user);
        PowerMock.expectPrivate(wsConnector, "fillUser", user2);
        PowerMock.replayAll();

        List<User> result = wsConnector.getAllUsers();

        PowerMock.verifyAll();
        assertSame(users, result);
    }

    @Test
    public void testGetUser() throws Exception {
        int userId = 5;
        wsConnector = PowerMock.createPartialMock(WebServicesConnector.class, "fillUser");
        Whitebox.setInternalState(wsConnector, "userManager", userManager);
        EasyMock.expect(userManager.get(userId)).andReturn(user);
        PowerMock.expectPrivate(wsConnector, "fillUser", user);
        PowerMock.replayAll();

        User result = wsConnector.getUser(userId);

        PowerMock.verifyAll();
        assertSame(user, result);
    }

    @Test
    public void testFillUser() throws Exception {
        user.setVoicemail(null);
        UserLine userLine = new UserLine();
        userLine.user_id = 5;
        userLine.line_id = 3;
        List<UserLine> userLines = new LinkedList<UserLine>();
        userLines.add(userLine);
        wsConnector =
                PowerMock.createPartialMockForAllMethodsExcept(WebServicesConnector.class,
                        "fillUser");
        EasyMock.expect(wsConnector.getCtiConfigForUser(user.getId())).andReturn(ctiConfig);
        EasyMock.expect(userManager.getUserLines(user.getId())).andReturn(userLines);
        lineManager.setLineAndIncall(user, userLine.line_id);
        EasyMock.expect(wsConnector.getVoicemailForUser(user.getId())).andReturn(voicemail);
        PowerMock.replayAll();
        Whitebox.setInternalState(wsConnector, "userManager", userManager);
        Whitebox.setInternalState(wsConnector, "lineManager", lineManager);

        Whitebox.invokeMethod(wsConnector, "fillUser", user);

        PowerMock.verifyAll();
        assertSame(voicemail, user.getVoicemail());
        assertSame(ctiConfig, user.getCtiConfiguration());
    }

    @Test
    public void testUpdateUser() throws Exception {
        userManager.update(user);
        userManager.updateCtiConfiguration(user);
        PowerMock.replayAll();

        wsConnector.updateUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testUpdateVoicemail() throws Exception {
        userManager.dissociateVoicemail(user);
        voicemailManager.update(voicemail);
        userManager.associateVoicemail(user);

        PowerMock.replayAll();

        this.wsConnector.updateVoicemail(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testCreateLine() throws Exception {
        lineManager.create(line);
        PowerMock.replayAll();

        wsConnector.createLine(line);

        PowerMock.verifyAll();
    }

    @Test
    public void testAssociateLineToUser() throws Exception {
        userManager.associateLine(user);
        PowerMock.replayAll();

        wsConnector.associateLineToUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testUpdateLine() throws Exception {
        lineManager.update(line);
        PowerMock.replayAll();

        this.wsConnector.updateLine(line);

        PowerMock.verifyAll();
    }

    @Test
    public void testDeleteLine() throws Exception {
        line.setDeviceId("00123-4564-56546546");
        devManager.resetAutoprov(EasyMock.isA(Device.class));
        lineManager.delete(line);
        PowerMock.replayAll();

        wsConnector.deleteLine(line);

        PowerMock.verifyAll();
    }

    @Test
    public void doNotResetAutoProvNoDevice() throws Exception {
        lineManager.delete(line);
        PowerMock.replayAll();

        wsConnector.deleteLine(line);

        PowerMock.verifyAll();
        PowerMock.verify();

    }

    @Test
    public void testGetLine() throws Exception {
        UserLine userLine = new UserLine();
        userLine.user_id = 5;
        userLine.line_id = 3;
        List<UserLine> userLines = new LinkedList<UserLine>();
        userLines.add(userLine);
        Line line = new Line();
        EasyMock.expect(userManager.getUserLines(5)).andReturn(userLines);
        EasyMock.expect(lineManager.get(3)).andReturn(line);
        PowerMock.replayAll();

        Line result = wsConnector.getLineForUser(5);

        PowerMock.verifyAll();
        assertSame(line, result);
    }

    @Test
    public void testGetLineNoLine() throws Exception {
        EasyMock.expect(userManager.getUserLines(5)).andReturn(new LinkedList<UserLine>());
        PowerMock.replayAll();

        Line result = wsConnector.getLineForUser(5);

        PowerMock.verifyAll();
        assertNull(result);
    }

    @Test
    public void testCreateVoicemailForUser() throws Exception {
        voicemailManager.create(voicemail);
        PowerMock.replayAll();

        wsConnector.createVoicemail(voicemail);

        PowerMock.verifyAll();
    }

    @Test
    public void testDissociateLineFromUser() throws Exception {
        Line line = new Line();
        line.setDeviceId("uhhbhbh");
        user.setLine(line);
        devManager.resetAutoprov(EasyMock.isA(Device.class));
        devManager.synchronize(EasyMock.isA(Device.class));
        userManager.dissociateLine(user);
        PowerMock.replayAll();

        wsConnector.dissociateLineFromUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testDissociateLineFromUser_autoprovFailsNotFound() throws Exception {
        Line line = new Line();
        line.setDeviceId("uhhbhbh");
        user.setLine(line);
        devManager.resetAutoprov(EasyMock.isA(Device.class));
        devManager.synchronize(EasyMock.isA(Device.class));
        EasyMock.expectLastCall().andThrow(new WebServicesException("test", 404));
        userManager.dissociateLine(user);
        PowerMock.replayAll();

        wsConnector.dissociateLineFromUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testDissociateLineFromUser_dissociateFailsNotFound() throws Exception {
        Line line = new Line();
        line.setDeviceId("uhhbhbh");
        user.setLine(line);
        devManager.resetAutoprov(EasyMock.isA(Device.class));
        devManager.synchronize(EasyMock.isA(Device.class));
        userManager.dissociateLine(user);
        EasyMock.expectLastCall().andThrow(new WebServicesException("test", 404));
        PowerMock.replayAll();

        wsConnector.dissociateLineFromUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testDissociateVoicemailFromUser() throws Exception {
        userManager.dissociateVoicemail(user);
        PowerMock.replayAll();

        wsConnector.dissociateVoicemailFromUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testDissociateVoicemailFromUser_dissociateFailsNotFound() throws Exception {
        userManager.dissociateVoicemail(user);
        EasyMock.expectLastCall().andThrow(new WebServicesException("test", 404));
        PowerMock.replayAll();

        wsConnector.dissociateVoicemailFromUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testDeleteUser() throws Exception {
        userManager.delete(user);
        PowerMock.replayAll();

        wsConnector.deleteUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testDeleteVoicemail() throws Exception {
        voicemailManager.delete(voicemail);
        PowerMock.replayAll();

        wsConnector.deleteVoicemail(voicemail);

        PowerMock.verifyAll();
    }

    @Test
    public void testGetUsersIdsAssociatedToVoicemail() throws Exception {
        UserVoicemail userVoicemail1 = new UserVoicemail();
        userVoicemail1.voicemail_id = 22;
        userVoicemail1.user_id = 101;
        UserVoicemail userVoicemail2 = new UserVoicemail();
        userVoicemail2.voicemail_id = 22;
        userVoicemail2.user_id = 250;
        List<UserVoicemail> voicemailLinks = Arrays.asList(userVoicemail1, userVoicemail2);

        EasyMock.expect(voicemailManager.getAssociatedUsersIds(123)).andReturn(voicemailLinks);
        PowerMock.replayAll();

        List<UserVoicemail> result = wsConnector.getUsersIdsAssociatedToVoicemail(123);

        PowerMock.verifyAll();
        assertSame(voicemailLinks, result);
    }

    @Test
    public void testAssociateVoicemailToUser() throws IOException, WebServicesException {
        userManager.associateVoicemail(user);
        PowerMock.replayAll();

        wsConnector.associateVoicemailToUser(user);

        PowerMock.verifyAll();
    }

    @Test
    public void testGetVoicemail() throws IOException, WebServicesException {
        UserVoicemail userVoicemail = new UserVoicemail();
        userVoicemail.user_id = 3;
        userVoicemail.voicemail_id = 4;
        EasyMock.expect(userManager.getUserVoicemail(3)).andReturn(userVoicemail);
        EasyMock.expect(voicemailManager.get(4)).andReturn(voicemail);
        PowerMock.replayAll();

        Voicemail result = wsConnector.getVoicemailForUser(3);

        PowerMock.verifyAll();
        assertSame(voicemail, result);
    }

    @Test
    public void testListVoicemail() throws IOException, WebServicesException {
        EasyMock.expect(voicemailManager.list()).andReturn(voicemails);
        PowerMock.replayAll();

        List<Voicemail> result = wsConnector.listVoicemails();

        PowerMock.verifyAll();
        assertSame(voicemails, result);
    }

    @Test
    public void testGetVoicemailNoVoicemail() throws IOException, WebServicesException {
        EasyMock.expect(userManager.getUserVoicemail(3)).andReturn(null);
        PowerMock.replayAll();

        Voicemail result = wsConnector.getVoicemailForUser(3);

        PowerMock.verifyAll();
        assertNull(result);
    }

    @Test
    public void testGetCtiConfigForUser() throws IOException, WebServicesException {
        UserCti userCti = new UserCti();
        userCti.cti_profile_id = 4;
        userCti.enabled = true;
        EasyMock.expect(userManager.getUserCti(3)).andReturn(userCti);
        EasyMock.expect(ctiProfileManager.getCtiProfileById(4)).andReturn(ctiProfile);
        PowerMock.replayAll();

        CtiConfiguration result = wsConnector.getCtiConfigForUser(3);

        PowerMock.verifyAll();
        assertSame(ctiProfile, result.getCtiProfile());
        assertTrue(result.isEnabled());
    }

    @Test
    public void testGetCtiConfigForUserNoCtiProfile() throws IOException,
            WebServicesException {
        UserCti userCti = new UserCti();
        userCti.enabled = false;
        userCti.cti_profile_id = null;
        EasyMock.expect(userManager.getUserCti(3)).andReturn(userCti);
        PowerMock.replayAll();

        CtiConfiguration result = wsConnector.getCtiConfigForUser(3);

        PowerMock.verifyAll();
        assertNull(result.getCtiProfile());
        assertFalse(result.isEnabled());
    }

    @Test
    public void testUpdateCtiConfig() throws WebServicesException, IOException {
        user.setCtiConfiguration(ctiConfig);
        userManager.updateCtiConfiguration(user);
        EasyMock.replay(userManager);

        wsConnector.updateCtiConfiguration(user);

        EasyMock.verify(userManager);
    }

    @Test
    public void testGetCtiProfileByName() throws IOException, WebServicesException {
        EasyMock.expect(ctiProfileManager.getCtiProfileByName("Client")).andReturn(ctiProfile);
        EasyMock.replay(ctiProfileManager);

        CtiProfile result = wsConnector.getCtiProfileByName("Client");

        EasyMock.verify(ctiProfileManager);
        assertSame(result, ctiProfile);
    }

    @Test
    public void testDisableLiveReload() throws IOException, WebServicesException {
        configManager.disableLiveReload();
        EasyMock.replay(configManager);

        wsConnector.disableLiveReload();

        EasyMock.verify(configManager);
    }

    @Test
    public void testEnableLiveReload() throws IOException, WebServicesException {
        configManager.enableLiveReload();
        EasyMock.replay(configManager);

        wsConnector.enableLiveReload();

        EasyMock.verify(configManager);
    }

    @Test
    public void testResetAutoprov() throws IOException, WebServicesException {
        Device dev = new Device("2345645465");
        devManager.resetAutoprov(dev);
        EasyMock.replay(devManager);

        wsConnector.resetAutoprov(dev);

        EasyMock.verify(devManager);
    }

    @Test
    public void testgetUsersNumber() throws Exception {
        int number = 51;
        EasyMock.expect(userManager.countUsers()).andReturn(number);
        EasyMock.replay(userManager);

        int res = wsConnector.getUsersNumber();

        assertEquals(number, res);
        EasyMock.verify(userManager);
    }

    @Test
    public void testGetInfo() throws Exception {
        Info info = new Info(UUID.randomUUID().toString());
        EasyMock.expect(configManager.getInfo()).andReturn(info);
        EasyMock.replay(configManager);

        Info res = wsConnector.getInfo();

        assertEquals(info, res);
        EasyMock.verify(configManager);
    }

    @Test
    public void testSynchronizeDevice() throws Exception {
        Device device = new Device("00123-4564-56546546");
        devManager.synchronize(device);
        PowerMock.replayAll();

        wsConnector.synchronizeDevice(device);

        PowerMock.verifyAll();
    }
}
